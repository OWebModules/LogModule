﻿using LogModule.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;

namespace LogModule.Factories
{
    public class LogStateFactory
    {
        public async Task<ResultItem<List<LogState>>> CreateLogstateList(List<clsOntologyItem> logStatesRaw, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<LogState>>>(() =>
            {
                var result = new ResultItem<List<LogState>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<LogState>()
                };

                result.Result = logStatesRaw.Where(logState => logState.GUID != globals.LState_Success.GUID && logState.GUID != globals.LState_Error.GUID).OrderBy(logState => logState.Name).Select(logState => new LogState
                {
                    IdLogState = logState.GUID,
                    NameLogState = logState.Name
                }).ToList();

                result.Result.Insert(0, new LogState
                {
                    IdLogState = globals.LState_Error.GUID,
                    NameLogState = globals.LState_Error.Name
                });

                result.Result.Insert(0, new LogState
                {
                    IdLogState = globals.LState_Success.GUID,
                    NameLogState = globals.LState_Success.Name
                });

                return result;
            });

            return taskResult;
        }
    }
}
