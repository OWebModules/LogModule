﻿using LogModule.Models;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Factories
{
    public static class ElasticSearchLogFactory
    {
        public static async Task<ResultItem<ElasticsearchLog>> CreateElasticSearchLog(GetElasticSearchLogModel model, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<ElasticsearchLog>>(() =>
             {
                 var result = new ResultItem<ElasticsearchLog>
                 {
                     ResultState = globals.LState_Success.Clone(),
                     Result = new ElasticsearchLog()
                 };

                 result.Result = new ElasticsearchLog
                 {
                     IdAttributeIsActive = model.IsActive.ID_Attribute,
                     IsActive = model.IsActive.Val_Bool.Value,
                     IdAttributeItemsCountBeforeLogging = model.CountBeforeIndex.ID_Attribute,
                     ItemsCountBeforeLogging = model.CountBeforeIndex.Val_Lng.Value,
                     IdEsIndex = model.LogToElasticSearchIndex.ID_Other,
                     EsIndex = model.LogToElasticSearchIndex.Name_Other,
                     IdEsType = model.LogToElasticSearchType.ID_Other,
                     EsType = model.LogToElasticSearchType.Name_Other,
                     IdEsPort = model.ServerPortToPort.ID_Other,
                     EsPort = long.Parse(model.ServerPortToPort.Name_Other),
                     IdEsServer = model.ServerPortToServer.ID_Other,
                     EsServer = model.ServerPortToServer.Name_Other
                 };

                 return result;
             });

            return taskResult;
        }
    }
}
