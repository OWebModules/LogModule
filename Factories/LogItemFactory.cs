﻿using LogModule.Models;
using LogModule.Services;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;

namespace LogModule.Factories
{
    public class LogItemFactory
    {
        public async Task<ResultItem<List<LogItem>>> CreateLogItemList(LogItemsRaw logItemsRaw, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<LogItem>>>(() =>
            {
                var result = new ResultItem<List<LogItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<LogItem>()
                };

                result.Result = (from logItem in logItemsRaw.LogEntries
                                 join logState in logItemsRaw.LogEntriesToLogStates on logItem.GUID equals logState.ID_Object into logStates
                                 from logState in logStates.DefaultIfEmpty()
                                 join user in logItemsRaw.LogEntriesToUsers on logItem.GUID equals user.ID_Object into users
                                 from user in users.DefaultIfEmpty()
                                 join stamp in logItemsRaw.DateTimeStamps on logItem.GUID equals stamp.ID_Object
                                 join message in logItemsRaw.Messages on logItem.GUID equals message.ID_Object into messages
                                 from message in messages.DefaultIfEmpty()
                                 select new LogItem
                                 {
                                     IdLogEntry = logItem.GUID,
                                     NameLogEntry = logItem.Name,
                                     IdLogState = logState?.ID_Other,
                                     NameLogState = logState?.Name_Other,
                                     IdUser = user?.ID_Other,
                                     NameUser = user?.Name_Other,
                                     IdAttributeDateTimeStamp = stamp.ID_Attribute,
                                     DateTimeStamp = stamp.Val_Date.Value,
                                     IdAttributeMessage = message?.ID_Attribute,
                                     Message = message?.Val_String,
                                     RelatedItems = logItemsRaw.RefItemsAndLogEntries.Where(rel => rel.ID_Object == logItem.GUID).ToList()
                                 }).ToList();

                return result;
            });

            return taskResult;
        }
    }
}
