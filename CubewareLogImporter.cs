﻿using ElasticSearchNestConnector;
using LogModule.Models;
using LogModule.Services;
using LogModule.Validation;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule;
using TextParserModule.Models;

namespace LogModule
{
    public class CubewareLogImporter
    {
        public Globals Globals { get; private set; }

        public async Task<clsOntologyItem> ImportCubewareLogs(ImportCubewareLogsRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = Globals.LState_Success.Clone();

                request.MessageOutput?.OutputInfo("Validate request...");
                result = ValidationController.ValidateImportCubewareLogsRequest(request, Globals);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get model...");

                var elasticAgent = new ServiceAgentElastic(Globals);

                var getModelResult = await elasticAgent.GetImportCubewareLogModel(request);

                result = getModelResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }

                
                if (getModelResult.Result.ConfigToExecuteMeasure.Val_Bit.Value)
                {
                    var model = new ImportCubewareLogsModel(getModelResult.Result, Globals);
                    var textParserController = new TextParserController(Globals, model);
                    textParserController.SavedMeasureDocs += TextParserController_SavedMeasureDocs;

                    var measureTextparserRequest = new MeasureTextParserRequest(getModelResult.Result.ConfigToTextParserMeasurement.ID_Other, request.CancellationToken)
                    {
                        MessageOutput = request.MessageOutput
                    };

                    result = await textParserController.MeasureTextParser(measureTextparserRequest);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        private clsOntologyItem TextParserController_SavedMeasureDocs(List<clsAppDocuments> documents, object sender)
        {
            var result = Globals.LState_Success.Clone();
            var model = (ImportCubewareLogsModel)sender;

            result = ImportDocuments(documents, model);

            return result;
        }
        
        private clsOntologyItem ImportDocuments(List<clsAppDocuments> documents, ImportCubewareLogsModel model)
        {
            var result = Globals.LState_Success.Clone();

            var objectsToCheck = new List<clsOntologyItem>();
            var relationsToSave = new List<clsObjectRel>();
            var attributesToSave = new List<clsObjectAtt>();

            var docs = documents.Where(doc => doc.Dict.ContainsKey(model.NameEndField)).ToList();
            var jobsDoc = docs.FirstOrDefault(doc => doc.Dict.ContainsKey(model.NameJobsCaseField)
                    && doc.Dict.ContainsKey(model.NameJobsDurationField)
                    && doc.Dict.ContainsKey(model.NameStartField)
                    && doc.Dict.ContainsKey(model.NameEndField));
            if (jobsDoc == null) return result;

            var jobsName = jobsDoc.Dict[model.NameJobsCaseField].ToString();
            var start = (DateTime)jobsDoc.Dict[model.NameStartField];
            var end = (DateTime)jobsDoc.Dict[model.NameEndField];

            var lastExecution = model.LastExecutions.FirstOrDefault(lastE => lastE.NameJob == jobsName);

            if (lastExecution != null)
            {
                docs = docs.Where(doc => ((DateTime)doc.Dict[model.NameEndField]) > lastExecution.DateTimeStamp).ToList();
            }
            if (!docs.Any()) return result;

            var lastEnd = docs.Max(doc => (DateTime)doc.Dict[model.NameEndField]);

            var jobRun = new clsOntologyItem
            {
                GUID = Globals.NewGUID,
                Name = jobsName,
                GUID_Parent = Cubeware.Config.LocalData.Class_Job_Run__Cubeware_.GUID,
                Type = Globals.Type_Object,
                New_Item = true
            };

            objectsToCheck.Add(jobRun);

            var attributeItems = new List<ObjectAttributeItem>();
            var relationItems = new List<ObjectRelationItem>();

            attributeItems.Add(new ObjectAttributeItem(jobRun, Cubeware.Config.LocalData.AttributeType_Start, start));
            attributeItems.Add(new ObjectAttributeItem(jobRun, Cubeware.Config.LocalData.AttributeType_Ende, end));
            relationItems.Add(new ObjectRelationItem(model.GetConfig(), jobRun, Cubeware.Config.LocalData.RelationType_contains, model.LastOrderId));
            model.LastOrderId++;

            var mainJob = objectsToCheck.FirstOrDefault(job => job.GUID_Parent == Cubeware.Config.LocalData.Class_Jobs__Cubeware_.GUID && job.Name == jobsName);
            if (mainJob == null)
            {
                mainJob = new clsOntologyItem
                {
                    Name = jobsName,
                    GUID_Parent = Cubeware.Config.LocalData.Class_Jobs__Cubeware_.GUID,
                    Type = Globals.Type_Object
                };
            }

            objectsToCheck.Add(mainJob);
            relationItems.Add(new ObjectRelationItem( jobRun, mainJob, Cubeware.Config.LocalData.RelationType_belongs_to));

            var jobDocs = documents.Where(doc => doc.Dict.ContainsKey(model.NameJobDurationField) 
                                              && doc.Dict.ContainsKey(model.NameJobField)
                                              && doc.Dict.ContainsKey(model.NameStartField)
                                              && doc.Dict.ContainsKey(model.NameEndField)).ToList();

            var jobNames = jobDocs.GroupBy(jobD => jobD.Dict[model.NameJobField].ToString()).Select(jobD => jobD.Key).ToList();

            var jobsPresent = objectsToCheck.Where(obj => obj.GUID_Parent == Cubeware.Config.LocalData.Class_Jobs__Cubeware_.GUID).ToList();
            var mappedJobs = model.MappedRessources.Where(map => map.GUID_Parent == Cubeware.Config.LocalData.Class_Jobs__Cubeware_.GUID).ToList();
            var jobSubs = (from jobNameNew in jobNames
                           join jobToCheck in jobsPresent on jobNameNew equals jobToCheck.Name into jobsToCheck1
                           from jobToCheck in jobsToCheck1.DefaultIfEmpty()
                           where jobToCheck == null
                           join mappedJob in mappedJobs on jobNameNew equals mappedJob.Name into mappedJobs1
                           from mappedJob in mappedJobs1.DefaultIfEmpty()
                           where mappedJob == null
                           select new clsOntologyItem
                           {
                               Name = jobNameNew,
                               GUID_Parent = Cubeware.Config.LocalData.Class_Jobs__Cubeware_.GUID,
                               Type = Globals.Type_Object
                           }).ToList();
            objectsToCheck.AddRange(jobSubs);
            jobSubs.AddRange(jobsPresent);
            jobSubs.AddRange(mappedJobs);

            foreach (var jobDocWithOItem in (from jobDoc in jobDocs
                                             join jobNameItem in jobSubs on jobDoc.Dict[model.NameJobField].ToString() equals jobNameItem.Name
                                             select new { jobDoc, jobNameItem }).ToList())
            { 
                var jobRunSubItem = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = jobDocWithOItem.jobNameItem.Name,
                    GUID_Parent = Cubeware.Config.LocalData.Class_Job_Run__Cubeware_.GUID,
                    Type = Globals.Type_Object,
                    New_Item = true
                };

                objectsToCheck.Add(jobDocWithOItem.jobNameItem);

                end = (DateTime)jobDocWithOItem.jobDoc.Dict[model.NameEndField];
                var duration = (double)jobDocWithOItem.jobDoc.Dict[model.NameJobDurationField];
                start = end.AddMilliseconds(-duration);
                objectsToCheck.Add(jobRunSubItem);
                attributeItems.Add(new ObjectAttributeItem(jobRunSubItem, Cubeware.Config.LocalData.AttributeType_Start, start));
                attributeItems.Add(new ObjectAttributeItem(jobRunSubItem, Cubeware.Config.LocalData.AttributeType_Ende, end));

                relationItems.Add(new ObjectRelationItem(jobRun, jobRunSubItem, Cubeware.Config.LocalData.RelationType_contains));
                relationItems.Add(new ObjectRelationItem(jobRunSubItem, jobDocWithOItem.jobNameItem, Cubeware.Config.LocalData.RelationType_belongs_to));
                relationItems.Add(new ObjectRelationItem(model.GetConfig(), jobRunSubItem, Cubeware.Config.LocalData.RelationType_contains, model.LastOrderId));
                model.LastOrderId++;
            }

            var scriptDocs = documents.Where(doc => doc.Dict.ContainsKey(model.NameScriptDurationField) 
                                                 && doc.Dict.ContainsKey(model.NameScriptField)
                                                 && doc.Dict.ContainsKey(model.NameStartField)
                                                 && doc.Dict.ContainsKey(model.NameEndField)).ToList();

            var scriptNames = scriptDocs.GroupBy(script => script.Dict[model.NameScriptField].ToString()).Select(script => script.Key).ToList();

            var scriptObjectsPresent = objectsToCheck.Where(obj => obj.GUID_Parent == Cubeware.Config.LocalData.Class_Scripts__Cubeware_.GUID).ToList();
            var mappedScripts = model.MappedRessources.Where(res => res.GUID_Parent == Cubeware.Config.LocalData.Class_Scripts__Cubeware_.GUID).ToList();
            var scriptNameItems = (from scriptNameNew in scriptNames
                                  join scriptToCheck in scriptObjectsPresent on scriptNameNew equals scriptToCheck.Name into scriptsToCheck1
                                  from scriptToCheck in scriptsToCheck1.DefaultIfEmpty()
                                  where scriptToCheck == null
                                  join mappedRessource in mappedScripts on scriptNameNew equals mappedRessource.Name into mappedRessources
                                  from mappedRessource in mappedRessources.DefaultIfEmpty()
                                  where mappedRessource == null
                                  select new clsOntologyItem
                                  {
                                      Name = scriptNameNew,
                                      GUID_Parent = Cubeware.Config.LocalData.Class_Scripts__Cubeware_.GUID,
                                      Type = Globals.Type_Object
                                  }).ToList();
            objectsToCheck.AddRange(scriptNameItems);
            scriptNameItems.AddRange(scriptObjectsPresent);
            scriptNameItems.AddRange(mappedScripts);

            foreach (var scriptDocWithOItem in (from scriptDoc in scriptDocs
                                             join scriptNameItem in scriptNameItems on scriptDoc.Dict[model.NameScriptField].ToString() equals scriptNameItem.Name
                                             select new { scriptDoc, scriptNameItem }).ToList())
            {
                var jobRunSubItem = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = scriptDocWithOItem.scriptNameItem.Name,
                    GUID_Parent = Cubeware.Config.LocalData.Class_Job_Run__Cubeware_.GUID,
                    Type = Globals.Type_Object,
                    New_Item = true
                };

                end = (DateTime)scriptDocWithOItem.scriptDoc.Dict[model.NameEndField];
                var duration = (double)scriptDocWithOItem.scriptDoc.Dict[model.NameScriptDurationField];
                start = end.AddMilliseconds(-duration);
                objectsToCheck.Add(jobRunSubItem);
                attributeItems.Add(new ObjectAttributeItem(jobRunSubItem, Cubeware.Config.LocalData.AttributeType_Start, start));
                attributeItems.Add(new ObjectAttributeItem(jobRunSubItem, Cubeware.Config.LocalData.AttributeType_Ende, end));

                relationItems.Add(new ObjectRelationItem(jobRun, jobRunSubItem, Cubeware.Config.LocalData.RelationType_contains));
                relationItems.Add(new ObjectRelationItem(jobRunSubItem, scriptDocWithOItem.scriptNameItem, Cubeware.Config.LocalData.RelationType_belonging_Resource));
                relationItems.Add(new ObjectRelationItem(model.GetConfig(), jobRunSubItem, Cubeware.Config.LocalData.RelationType_contains, model.LastOrderId));
                model.LastOrderId++;
            }

            var mappingDocs = documents.Where(doc => doc.Dict.ContainsKey(model.NameMappingDurtaionField) 
                                                  && doc.Dict.ContainsKey(model.NameMappingField)
                                                  && doc.Dict.ContainsKey(model.NameStartField)
                                                  && doc.Dict.ContainsKey(model.NameEndField)).ToList();

            var mappingNames = mappingDocs.GroupBy(script => script.Dict[model.NameMappingField].ToString()).Select(script => script.Key).ToList();

            var mappingsPesent = objectsToCheck.Where(obj => obj.GUID_Parent == Cubeware.Config.LocalData.Class_Mapping__Cubeware_.GUID).ToList();
            var mappedMappings = model.MappedRessources.Where(res => res.GUID_Parent == Cubeware.Config.LocalData.Class_Mapping__Cubeware_.GUID).ToList();

            var mappingNameItems = (from mappingNameNew in mappingNames
                                    join mappingToCheck in mappingsPesent on mappingNameNew equals mappingToCheck.Name into mappingsToCheck1
                                    from mappingToCheck in mappingsToCheck1.DefaultIfEmpty()
                                    where mappingToCheck == null
                                    join mappedMapping in mappedMappings on mappingNameNew equals mappedMapping.Name into mappedMappings1
                                    from mappedMapping in mappedMappings1.DefaultIfEmpty()
                                    where mappedMapping == null
                                    select new clsOntologyItem
                                    {
                                        Name = mappingNameNew,
                                        GUID_Parent = Cubeware.Config.LocalData.Class_Mapping__Cubeware_.GUID,
                                        Type = Globals.Type_Object
                                    }).ToList();
            objectsToCheck.AddRange(mappingNameItems);
            mappingNameItems.AddRange(mappingsPesent);
            mappingNameItems.AddRange(mappedMappings);

            foreach (var mappingDocWithOItem in (from mappingDoc in mappingDocs
                                                join mappingNameItem in mappingNameItems on mappingDoc.Dict[model.NameMappingField].ToString() equals mappingNameItem.Name
                                                select new { mappingDoc, mappingNameItem }).ToList())
            {
                var jobRunSubItem = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = mappingDocWithOItem.mappingNameItem.Name,
                    GUID_Parent = Cubeware.Config.LocalData.Class_Job_Run__Cubeware_.GUID,
                    Type = Globals.Type_Object,
                    New_Item = true
                };

                end = (DateTime)mappingDocWithOItem.mappingDoc.Dict[model.NameEndField];
                var duration = (double)mappingDocWithOItem.mappingDoc.Dict[model.NameMappingDurtaionField];
                start = end.AddMilliseconds(-duration);
                objectsToCheck.Add(jobRunSubItem);
                attributeItems.Add(new ObjectAttributeItem(jobRunSubItem, Cubeware.Config.LocalData.AttributeType_Start, start));
                attributeItems.Add(new ObjectAttributeItem(jobRunSubItem, Cubeware.Config.LocalData.AttributeType_Ende, end));

                relationItems.Add(new ObjectRelationItem(jobRun, jobRunSubItem, Cubeware.Config.LocalData.RelationType_contains));
                relationItems.Add(new ObjectRelationItem(jobRunSubItem, mappingDocWithOItem.mappingNameItem, Cubeware.Config.LocalData.RelationType_belonging_Resource));
                relationItems.Add(new ObjectRelationItem(model.GetConfig(), jobRunSubItem, Cubeware.Config.LocalData.RelationType_contains, model.LastOrderId));
                model.LastOrderId++;
            }

            var cwStepDocs = documents.Where(doc => doc.Dict.ContainsKey(model.NameCWStepDurationField) 
                                          && doc.Dict.ContainsKey(model.NameCWStepField)
                                          && doc.Dict.ContainsKey(model.NameStartField)
                                          && doc.Dict.ContainsKey(model.NameEndField)).ToList();

            var cwStepNames = cwStepDocs.GroupBy(script => script.Dict[model.NameCWStepField].ToString()).Select(script => script.Key).ToList();

            var cwStepsPresent = objectsToCheck.Where(obj => obj.GUID_Parent == Cubeware.Config.LocalData.Class_CW_Step__Cubeware_.GUID).ToList();
            var mappedCWSteps = model.MappedRessources.Where(res => res.GUID_Parent == Cubeware.Config.LocalData.Class_CW_Step__Cubeware_.GUID).ToList();
            var cwStepNameItems = (from cwStepNameNew in cwStepNames
                                   join cwStepToCheck in cwStepsPresent on cwStepNameNew equals cwStepToCheck.Name into cwStepsToCheck1
                                   from cwStepToCheck in cwStepsToCheck1.DefaultIfEmpty()
                                   where cwStepToCheck == null
                                   join mappedCWStep in mappedCWSteps on cwStepNameNew equals mappedCWStep.Name into mappedCWSteps1
                                   from mappedCWStep in mappedCWSteps1.DefaultIfEmpty()
                                   where mappedCWStep == null
                                   select new clsOntologyItem
                                   {
                                       Name = cwStepNameNew,
                                       GUID_Parent = Cubeware.Config.LocalData.Class_CW_Step__Cubeware_.GUID,
                                       Type = Globals.Type_Object
                                   }).ToList();
            objectsToCheck.AddRange(cwStepNameItems);
            cwStepNameItems.AddRange(cwStepsPresent);
            cwStepNameItems.AddRange(mappedCWSteps);

            foreach (var cwStepDocWithOItem in (from cwStepDoc in cwStepDocs
                                                 join cwStepNameItem in cwStepNameItems on cwStepDoc.Dict[model.NameCWStepField].ToString() equals cwStepNameItem.Name
                                                 select new { cwStepDoc, cwStepNameItem }).ToList())
            {
                var jobRunSubItem = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = cwStepDocWithOItem.cwStepNameItem.Name,
                    GUID_Parent = Cubeware.Config.LocalData.Class_Job_Run__Cubeware_.GUID,
                    Type = Globals.Type_Object,
                    New_Item = true
                };

                end = (DateTime)cwStepDocWithOItem.cwStepDoc.Dict[model.NameEndField];
                var duration = (double)cwStepDocWithOItem.cwStepDoc.Dict[model.NameCWStepDurationField];
                start = end.AddMilliseconds(-duration);
                objectsToCheck.Add(jobRunSubItem);
                attributeItems.Add(new ObjectAttributeItem(jobRunSubItem, Cubeware.Config.LocalData.AttributeType_Start, start));
                attributeItems.Add(new ObjectAttributeItem(jobRunSubItem, Cubeware.Config.LocalData.AttributeType_Ende, end));

                relationItems.Add(new ObjectRelationItem(jobRun, jobRunSubItem, Cubeware.Config.LocalData.RelationType_contains));
                relationItems.Add(new ObjectRelationItem(jobRunSubItem, cwStepDocWithOItem.cwStepNameItem, Cubeware.Config.LocalData.RelationType_belonging_Resource));
                relationItems.Add(new ObjectRelationItem(model.GetConfig(), jobRunSubItem, Cubeware.Config.LocalData.RelationType_contains, model.LastOrderId));
                model.LastOrderId++;
            }

            var errorDocs = documents.Where(doc => doc.Dict.ContainsKey(model.NameErrorField)).ToList();
            attributeItems.Add(new ObjectAttributeItem(jobRun, Cubeware.Config.LocalData.AttributeType_Has_Error, errorDocs.Any()));

            if (lastExecution == null)
            {
                var lastExecutionItem = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = mainJob.Name,
                    GUID_Parent = Cubeware.Config.LocalData.Class_Last_MainJob_Import.GUID,
                    Type = Globals.Type_Object
                };
                objectsToCheck.Add(lastExecutionItem);
                var lastExecutionAtt = model.RelationConfig.Rel_ObjectAttribute(lastExecutionItem, Cubeware.Config.LocalData.AttributeType_Last_DateTimeStamp, lastEnd, idAttribute: Globals.NewGUID);

                attributesToSave.Add(lastExecutionAtt);
                relationItems.Add(new ObjectRelationItem(lastExecutionItem, mainJob, Cubeware.Config.LocalData.RelationType_belongs_to));
                relationItems.Add(new ObjectRelationItem(model.GetConfig(), lastExecutionItem, Cubeware.Config.LocalData.RelationType_contains, model.LastOrderId));
                model.LastOrderId++;

                lastExecution = new LastExecuteItem(lastExecutionItem,
                       mainJob,
                       lastExecutionAtt);
                model.LastExecutions.Add(lastExecution);
            }
            else
            {
                lastExecution.LastExecutionAtt.Val_Date = lastEnd;
                lastExecution.LastExecutionAtt.Val_Name = lastEnd.ToString();
                attributesToSave.Add(lastExecution.LastExecutionAtt);
            }

            if (objectsToCheck.Any())
            {
                var checkResult = model.ServiceAgentElastic.CheckObjectsSync(objectsToCheck);
                result = checkResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            relationsToSave = relationItems.Select(rel => model.RelationConfig.Rel_ObjectRelation(rel.ObjectLeft, rel.OItemRight, rel.ORelationTypeItem, orderId: rel.OrderId)).ToList();

            if (relationsToSave.Any())
            {
                result = model.ServiceAgentElastic.SaveRelationsSync(relationsToSave);
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            attributesToSave.AddRange(attributeItems.Select(rel => model.RelationConfig.Rel_ObjectAttribute(rel.ObjectItem, rel.AttributeTypeItem, rel.Value)));

            if (attributesToSave.Any())
            {
                result = model.ServiceAgentElastic.SaveAttributesSync(attributesToSave);
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            return result;
        }

        public CubewareLogImporter(Globals globals)
        {
            Globals = globals;
        }
    }
}
