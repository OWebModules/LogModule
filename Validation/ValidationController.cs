﻿using LogModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateSyncTextParserToApplicationInsightsRequest(SyncTextParserToApplicationInsightsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The configuration-id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The configuration-id is no valid id (guid)!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateTextParserToApplicationInsightsModel(TextParserToApplicationInsightsModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.RootConfig))
            {
                if (model.RootConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Root-Config is not found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.ConfigsToInstrumentationKeys))
            {
                if (!model.ConfigsToInstrumentationKeys.Any() || model.Configs.Count != model.ConfigsToInstrumentationKeys.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Count of Configs ({model.Configs.Count}) is different to count of instrumentation-keys ({model.ConfigsToInstrumentationKeys.Count})";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.ConfigsToTextParsers))
            {
                if (!model.ConfigsToTextParsers.Any() || model.Configs.Count != model.ConfigsToTextParsers.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Count of Configs ({model.Configs.Count}) is different to count of instrumentation-keys ({model.ConfigsToTextParsers.Count})";
                    return result;
                }
            }

            return result;
        }
        public static clsOntologyItem ValidateSyncLogEntriesToAppInsightRequest(SyncLogEntriesToAppInsightRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The configuration-id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The configuration-id is no valid id (guid)!";
                return result;
            }

            if (string.IsNullOrEmpty(request.IdMasterUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The masteruser-id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdMasterUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The masteruser-id is no valid id (guid)!";
                return result;
            }

            if (string.IsNullOrEmpty(request.Password))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The password is empty!";
                return result;
            }
            return result;
        }

        public static clsOntologyItem ValidateGetLogentriesToApplicationInsightsModelResult(GetLogentriesToApplicationInsightsModelResult modelResult, Globals globals, string property = null)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(property) || property == nameof(modelResult.RootConfig))
            {
                if (modelResult.RootConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Root-Config!";
                    return result;
                }
            }
            if(string.IsNullOrEmpty(property) || property == nameof(modelResult.InstrumentationKeysOfConfigs))
            {
                if (modelResult.InstrumentationKeysOfConfigs.Count != modelResult.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Missing InstrumentationKeys";
                    return result;
                }
            }
            if (string.IsNullOrEmpty(property) || property == nameof(modelResult.EventLogsOfConfigs))
            {
                if (modelResult.InstrumentationKeysOfConfigs.Count < modelResult.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Missing Logs";
                    return result;
                }
            }


            return result;
        }

        public static clsOntologyItem ValidateGetElasticSearchLogRequest(GetElasticSearchLogRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdReference))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Reference-Id is empty!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetElasticSearchLogModel(GetElasticSearchLogModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetElasticSearchLogModel.Reference))
            {
                if (model.Reference == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Reference found!";
                    return result;
                }
            }

            if (propertyName == nameof(GetElasticSearchLogModel.LogOfReference))
            {
                if (model.LogOfReference == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No relation between Reference and Log-Entry found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetElasticSearchLogModel.ElasticSearchLog))
            {
                if (model.ElasticSearchLog == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Log-Item found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetElasticSearchLogModel.CountBeforeIndex))
            {
                if (model.CountBeforeIndex == null || model.CountBeforeIndex.Val_Lng == null ||
                    model.CountBeforeIndex.Val_Lng.Value < 1 || model.CountBeforeIndex.Val_Lng.Value > 5000)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "ItemCountBeforeLogging must be set to a number between 0 and 5000!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetElasticSearchLogModel.LogToElasticSearchIndex))
            {
                if (model.LogToElasticSearchIndex == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must reference a ElasticSearchIndex!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetElasticSearchLogModel.LogToElasticSearchType))
            {
                if (model.LogToElasticSearchType == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must reference a ElasticSearchType!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetElasticSearchLogModel.ElasticSearchIndexToServerPort))
            {
                if (model.ElasticSearchIndexToServerPort == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must reference a ServerPort to ElasticSearchIndex!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetElasticSearchLogModel.ServerPortToServer))
            {
                if (model.ServerPortToServer == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must reference a Server to the ServerPort!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(GetElasticSearchLogModel.ServerPortToPort))
            {
                if (model.ServerPortToPort == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You must reference a Port to the ServerPort!";
                    return result;
                }

                long tryVal = 0;
                if (!long.TryParse(model.ServerPortToPort.Name_Other, out tryVal))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Port has no valid value: {model.ServerPortToPort.Name_Other}";
                    return result;
                }
            }



            return result;
        }

        public static clsOntologyItem ValidateCheckUrlsRequest(CheckUrlsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateCheckurlsModel(CheckUrlsModel model, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (model.Config == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No valid configuration!";
                return result;
            }

            if (model.Config.GUID_Parent != UrlCheck.Config.LocalData.Class_UrlCheck.GUID)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Config is of the wrong class!";
                return result;
            }
            
            return result;
        }

        public static clsOntologyItem ValidateImportCubewareLogsRequest(ImportCubewareLogsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetImportCubewareLogsModel(ImportCubewareLogsModelRaw model, OntologyModDBConnector dbReader, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(ImportCubewareLogsModelRaw.Config))
            {
                if (dbReader.Objects1.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one config, provided are {dbReader.Objects1.Count} objects!";
                    return result;
                }

                model.Config = dbReader.Objects1.First();

                if (model.Config.GUID_Parent != Cubeware.Config.LocalData.Class_Cubeware_Logimporter.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided object is not of class {Cubeware.Config.LocalData.Class_Cubeware_Logimporter.Name}";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToExecuteMeasure))
            {
                if (dbReader.ObjAtts.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute measure attribute, provided are {dbReader.ObjAtts.Count} attributes!";
                    return result;
                }

                model.ConfigToExecuteMeasure = dbReader.ObjAtts.First();

                if (model.ConfigToExecuteMeasure.ID_AttributeType != Cubeware.Config.LocalData.AttributeType_Execute_Measure.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided Attribute is not of Attribute-Type {Cubeware.Config.LocalData.AttributeType_Execute_Measure.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToLastExecutes))
            {
                model.ConfigToLastExecutes = dbReader.ObjectRels;
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.LastExecutesToDateTimeStamp))
            {
                if (dbReader.ObjAtts.Count != model.ConfigToLastExecutes.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {dbReader.ObjAtts.Count} attributes, but you need {model.ConfigToLastExecutes.Count} attributes!";
                    return result;
                }

                model.LastExecutesToDateTimeStamp = dbReader.ObjAtts;
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.LastExecutesToJobs))
            {
                if (dbReader.ObjectRels.Count != model.ConfigToLastExecutes.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {dbReader.ObjectRels.Count} job-relations, but you need {model.ConfigToLastExecutes.Count} job-relations!";
                    return result;
                }

                model.LastExecutesToJobs = dbReader.ObjectRels;
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToJobsDurationField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Job-Duration Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToJobsDurationField = dbReader.ObjectRels.First();

                if (model.ConfigToJobsDurationField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Jobs_Duration_Field_Field.ID_RelationType ||
                    model.ConfigToJobsDurationField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Jobs_Duration_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided Attribute is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Jobs_Duration_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToCWStepField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute CWStep Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToCWStepField = dbReader.ObjectRels.First();

                if (model.ConfigToCWStepField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_CWStep_Field_Field.ID_RelationType ||
                    model.ConfigToCWStepField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_CWStep_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_CWStep_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToJobsCaseField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Jobs Case Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToJobsCaseField = dbReader.ObjectRels.First();

                if (model.ConfigToJobsCaseField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Jobs_Case_Field_Field.ID_RelationType ||
                    model.ConfigToJobsCaseField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Jobs_Case_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Jobs_Case_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToJobField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Jobs Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToJobField = dbReader.ObjectRels.First();

                if (model.ConfigToJobField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Job_Field_Field.ID_RelationType ||
                    model.ConfigToJobField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Job_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Job_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToStartFieldField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Start Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToStartFieldField = dbReader.ObjectRels.First();

                if (model.ConfigToStartFieldField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Start_Field_Field.ID_RelationType ||
                    model.ConfigToStartFieldField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Start_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Start_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToJobDurationField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Job Duration Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToJobDurationField = dbReader.ObjectRels.First();

                if (model.ConfigToJobDurationField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Job_Duration_Field_Field.ID_RelationType ||
                    model.ConfigToJobDurationField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Job_Duration_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Job_Duration_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToScriptField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Script Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToScriptField = dbReader.ObjectRels.First();

                if (model.ConfigToScriptField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Script_Field_Field.ID_RelationType ||
                    model.ConfigToScriptField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Script_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Script_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToMappingDurationField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Mapping Duration Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToMappingDurationField = dbReader.ObjectRels.First();

                if (model.ConfigToMappingDurationField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Mapping_Duration_Field_Field.ID_RelationType ||
                    model.ConfigToMappingDurationField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Mapping_Duration_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Mapping_Duration_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToEndField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute End Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToEndField = dbReader.ObjectRels.First();

                if (model.ConfigToEndField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_End_Field_Field.ID_RelationType ||
                    model.ConfigToEndField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_End_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_End_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToScriptDurationField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Script-Duration Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToScriptDurationField = dbReader.ObjectRels.First();

                if (model.ConfigToScriptDurationField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Script_Duration_Field_Field.ID_RelationType ||
                    model.ConfigToScriptDurationField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Script_Duration_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Script_Duration_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToCWStepDurationField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute CWStep-Duration Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToCWStepDurationField = dbReader.ObjectRels.First();

                if (model.ConfigToCWStepDurationField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_CWStep_Duration_Field_Field.ID_RelationType ||
                    model.ConfigToCWStepDurationField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_CWStep_Duration_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_CWStep_Duration_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToMappingField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Mapping Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToMappingField = dbReader.ObjectRels.First();

                if (model.ConfigToMappingField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Mapping_Field_Field.ID_RelationType ||
                    model.ConfigToMappingField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Mapping_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Mapping_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToErrorField))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Error Field, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToErrorField = dbReader.ObjectRels.First();

                if (model.ConfigToErrorField.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Error_Field_Field.ID_RelationType ||
                    model.ConfigToErrorField.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Error_Field_Field.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_Error_Field.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.ConfigToTextParserMeasurement))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one execute Textparser Measurement, provided are {dbReader.ObjectRels.Count}!";
                    return result;
                }

                model.ConfigToTextParserMeasurement = dbReader.ObjectRels.First();

                if (model.ConfigToTextParserMeasurement.ID_RelationType != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_belonging_Source_Textparser_Measurement.ID_RelationType ||
                    model.ConfigToTextParserMeasurement.ID_Parent_Other != Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_belonging_Source_Textparser_Measurement.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is not a {Cubeware.Config.LocalData.Class_Field.Name} or not a {Cubeware.Config.LocalData.RelationType_belonging.Name}!";
                    return result;
                }
            }
            else if (propertyName == nameof(ImportCubewareLogsModelRaw.MappedRessources))
            {
                model.MappedRessources = dbReader.ObjectRels.Select(map => new clsOntologyItem
                {
                    GUID = map.ID_Other,
                    Name = map.Name_Other,
                    GUID_Parent = map.ID_Parent_Other,
                    Type = map.Ontology
                }).ToList();
            }

            return result;
        }

    }
}
