﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class LogState
    {
        public string IdLogState { get; set; }
        public string NameLogState { get; set; }
    }
}
