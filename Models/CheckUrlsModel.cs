﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;

namespace LogModule.Models
{
    public class CheckUrlsModel
    {
        public clsOntologyItem Config { get; set; }
        public List<clsObjectRel> ConfigsToUrls { get; set; } = new List<clsObjectRel>();
    }
}
