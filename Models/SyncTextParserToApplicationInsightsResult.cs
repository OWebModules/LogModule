﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class SyncTextParserToApplicationInsightsResult
    {
        public long DocumentCountSynced { get; set; }
    }
}
