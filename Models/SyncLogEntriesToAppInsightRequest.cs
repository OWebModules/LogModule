﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class SyncLogEntriesToAppInsightRequest
    {
        public string IdConfig { get; set; }
        public string IdMasterUser { get; set; }
        public string Password { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        

        public SyncLogEntriesToAppInsightRequest(string idConfig, string idMasterUser, string password)
        {
            IdConfig = idConfig;
            IdMasterUser = idMasterUser;
            Password = password;
        }
    }
}
