﻿using LogModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class ImportCubewareLogsModel
    {
        private ImportCubewareLogsModelRaw rawModel;

        public string IdConfig { get; set; }

        public string NameConfig { get; set; }

        public string IdExecuteMeasure { get; set; }
        public bool ExecuteMeasure { get; set; }
        public List<LastExecuteItem> LastExecutions { get; set; } = new List<LastExecuteItem>();

        public string IdJobsDurationField { get; set; }

        public string NameJobsDurationField { get; set; }

        public string IdCWStepField { get; set; }

        public string NameCWStepField { get; set; }

        public string IdJobsCaseField { get; set; }

        public string NameJobsCaseField { get; set; }

        public string IdJobField { get; set; }
        public string NameJobField { get; set; }

        public string IdStartField { get; set; }
        public string NameStartField { get; set; }

        public string IdJobDurationField { get; set; }
        public string NameJobDurationField { get; set; }

        public string IdScriptField { get; set; }
        public string NameScriptField { get; set; }

        public string IdMappingDurationField { get; set; }

        public string NameMappingDurtaionField { get; set; }

        public string IdEndField { get; set; }

        public string NameEndField { get; set; }

        public string IdScriptDurationField { get; set; }

        public string NameScriptDurationField { get; set; }

        public string IdCWStepDurationField { get; set; }

        public string NameCWStepDurationField { get; set; }

        public string IdMappingField { get; set; }
        public string NameMappingField { get; set; }

        public string IdErrorField { get; set; }

        public string NameErrorField { get; set; }

        public string IdTextParser { get; set; }
        public string NameTextParser { get; set; }

        public ServiceAgentElastic ServiceAgentElastic { get; set; }

        public clsRelationConfig RelationConfig { get; set; }

        public long LastOrderId { get; set; } = 1;

        public List<clsOntologyItem> MappedRessources { get; set; } = new List<clsOntologyItem>();

        public clsOntologyItem GetConfig()
        {
            return rawModel.Config;
        }

        public ImportCubewareLogsModel(ImportCubewareLogsModelRaw rawModel, Globals globals)
        {
            this.rawModel = rawModel;

            IdConfig = rawModel.Config.GUID;
            NameConfig = rawModel.Config.Name;
            IdJobsDurationField = rawModel.ConfigToJobsDurationField.ID_Other;
            NameJobsDurationField = rawModel.ConfigToJobsDurationField.Name_Other;
            IdExecuteMeasure = rawModel.ConfigToExecuteMeasure.ID_Object;
            ExecuteMeasure = rawModel.ConfigToExecuteMeasure.Val_Bit.Value;
            LastExecutions = (from configToLastExecute in rawModel.ConfigToLastExecutes
                              join lastexecutionStamp in rawModel.LastExecutesToDateTimeStamp on configToLastExecute.ID_Other equals lastexecutionStamp.ID_Object
                              join lastexecutionJob in rawModel.LastExecutesToJobs on configToLastExecute.ID_Other equals lastexecutionJob.ID_Object
                              select new LastExecuteItem(new clsOntologyItem
                              {
                                  GUID = configToLastExecute.ID_Other,
                                  Name = configToLastExecute.Name_Other,
                                  GUID_Parent = configToLastExecute.ID_Parent_Other,
                                  Type = configToLastExecute.Ontology
                              },
                              new clsOntologyItem
                              {
                                  GUID = configToLastExecute.ID_Other,
                                  Name = configToLastExecute.Name_Other,
                                  GUID_Parent = configToLastExecute.ID_Parent_Other,
                                  Type = configToLastExecute.Ontology
                              }, lastexecutionStamp)).ToList();
            IdCWStepField = rawModel.ConfigToCWStepField.ID_Other;
            NameCWStepField = rawModel.ConfigToCWStepField.Name_Other;
            IdJobsCaseField = rawModel.ConfigToJobsCaseField.ID_Other;
            NameJobsCaseField = rawModel.ConfigToJobsCaseField.Name_Other;
            IdJobField = rawModel.ConfigToJobField.ID_Other;
            NameJobField = rawModel.ConfigToJobField.Name_Other;
            IdScriptField = rawModel.ConfigToScriptField.ID_Other;
            NameScriptField = rawModel.ConfigToScriptField.Name_Other;
            IdMappingDurationField = rawModel.ConfigToMappingDurationField.ID_Other;
            NameMappingDurtaionField = rawModel.ConfigToMappingDurationField.Name_Other;
            IdEndField = rawModel.ConfigToEndField.ID_Other;
            NameEndField = rawModel.ConfigToEndField.Name_Other;
            IdScriptDurationField = rawModel.ConfigToScriptDurationField.ID_Other;
            NameScriptDurationField = rawModel.ConfigToScriptDurationField.Name_Other;
            IdCWStepDurationField = rawModel.ConfigToCWStepDurationField.ID_Other;
            NameCWStepDurationField = rawModel.ConfigToCWStepDurationField.Name_Other;
            IdMappingField = rawModel.ConfigToMappingField.ID_Other;
            NameMappingField = rawModel.ConfigToMappingField.Name_Other;
            IdErrorField = rawModel.ConfigToErrorField.ID_Other;
            NameErrorField = rawModel.ConfigToErrorField.Name_Other;
            IdTextParser = rawModel.ConfigToTextParserMeasurement.ID_Other;
            NameTextParser = rawModel.ConfigToTextParserMeasurement.Name_Other;
            IdStartField = rawModel.ConfigToStartFieldField.ID_Other;
            NameStartField = rawModel.ConfigToStartFieldField.Name_Other;
            IdJobDurationField = rawModel.ConfigToJobDurationField.ID_Other;
            NameJobDurationField = rawModel.ConfigToJobDurationField.Name_Other;
            MappedRessources = rawModel.MappedRessources;

            RelationConfig = new clsRelationConfig(globals);
            ServiceAgentElastic = new ServiceAgentElastic(globals);
        }
    }

    public class LastExecuteItem
    {
        public string IdLastExecute 
        {
            get { return LastExecute?.GUID; }
        }

        public string NameLastExecute 
        {
            get { return LastExecute?.Name; }
        }

        public string IdAttributeDateTimeStamp 
        { 
            get { return LastExecutionAtt?.ID_Attribute; }
        }

        public DateTime? DateTimeStamp 
        { 
            get { return LastExecutionAtt?.Val_Datetime; }
        }

        public string IdJob 
        { 
            get { return MainJob?.GUID; }
        }

        public string NameJob 
        { 
            get { return MainJob?.Name; }
        }

        public clsOntologyItem LastExecute { get; private set; }
        public clsOntologyItem MainJob { get; private set; }

        public clsObjectAtt LastExecutionAtt { get; private set; }

        public LastExecuteItem(clsOntologyItem lastExecute,
            clsOntologyItem mainJob,
            clsObjectAtt lastExecutionAtt)
        {
            MainJob = mainJob;
            LastExecutionAtt = lastExecutionAtt;
            LastExecute = lastExecute;
        }
    }
}
