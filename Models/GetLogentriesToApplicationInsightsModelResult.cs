﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class GetLogentriesToApplicationInsightsModelResult
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectAtt> DeleteLogsOfConfigs { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> InstrumentationKeysOfConfigs { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> DomainsOfConfigs { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> EventLogsOfConfigs { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> HostsOfConfigs { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> UsersOfConfigs { get; set; } = new List<clsObjectRel>();
    }
}
