﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class SyncTextParserToApplicationInsightsRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public SyncTextParserToApplicationInsightsRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
