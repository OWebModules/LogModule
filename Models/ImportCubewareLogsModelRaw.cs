﻿using LogModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class ImportCubewareLogsModelRaw
    {
        public clsOntologyItem Config { get; set; }
        public clsObjectAtt ConfigToExecuteMeasure { get; set; }

        public List<clsObjectRel> ConfigToLastExecutes { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> LastExecutesToDateTimeStamp { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> LastExecutesToJobs { get; set; } = new List<clsObjectRel>();

        public DateTime CurrentExecutionStamp { get; set; }

        public clsObjectRel ConfigToJobsDurationField { get; set; }

        public clsObjectRel ConfigToCWStepField { get; set; }

        public clsObjectRel ConfigToJobsCaseField { get; set; }

        public clsObjectRel ConfigToJobField { get; set; }

        public clsObjectRel ConfigToStartFieldField { get; set; }

        public clsObjectRel ConfigToJobDurationField { get; set; }

        public clsObjectRel ConfigToScriptField { get; set; }

        public clsObjectRel ConfigToMappingDurationField { get; set; }

        public clsObjectRel ConfigToEndField { get; set; }

        public clsObjectRel ConfigToScriptDurationField { get; set; }

        public clsObjectRel ConfigToCWStepDurationField { get; set; }

        public clsObjectRel ConfigToMappingField { get; set; }

        public clsObjectRel ConfigToErrorField { get; set; }

        public clsObjectRel ConfigToTextParserMeasurement { get; set; }

        public List<clsOntologyItem> ObjectsToCheck { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> RelationsToSave { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> AttributesToSave { get; set; } = new List<clsObjectAtt>();

        public List<clsOntologyItem> MappedRessources { get; set; } = new List<clsOntologyItem>();

        public ServiceAgentElastic ServiceAgentElastic { get; set; }
        public clsRelationConfig RelationConfig { get; set;  }
    }
}
