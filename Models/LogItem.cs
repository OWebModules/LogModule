﻿using OntologyClasses.BaseClasses;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    [KendoGridConfig(width = "100%", groupbable = true, autoBind = true, selectable = SelectableType.cell, scrollable = true)]
    [KendoPageable(refresh = true, pageSizes = new int[] { 5, 10, 15, 20, 50, 100, 1000 }, buttonCount = 5, pageSize = 20)]
    [KendoSortable(mode = SortType.multiple, showIndexes = true)]
    [KendoFilter(extra = true)]
    [KendoStringFilterable(contains = "Contains", startsWith = "Starts with", eq = "Is equal to", neq = "Is not equal to", isnotnull = "Not null", isnull = "Null", isempty = "Empty")]
    [KendoDateFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    public class LogItem
    {
        [KendoColumn(hidden = true)]
        public string IdLogEntry { get; set; }
        [KendoColumn(hidden = true)]
        public string NameLogEntry { get; set; }
        [KendoColumn(hidden = true)]
        public string IdLogState { get; set; }
        [KendoColumn(hidden = false, title = "Logstate", Order = 0, filterable = true)]
        public string NameLogState { get; set; }
        [KendoColumn(hidden = true)]
        public string IdAttributeDateTimeStamp { get; set; }
        [KendoColumn(hidden = false, title = "Datetime", Order = 1, filterable = true, template = "#= DateTimeStamp != null ? kendo.toString(kendo.parseDate(DateTimeStamp), 'dd.MM.yyyy hh:mm:ss') : '' #", type = ColType.DateType)]
        public DateTime? DateTimeStamp { get; set; }
        [KendoColumn(hidden = true)]
        public string IdAttributeMessage { get; set; }
        [KendoColumn(hidden = false, title = "Message", Order = 2, filterable = true)]
        public string Message { get; set; }
        [KendoColumn(hidden = true)]
        public string IdUser { get; set; }
        [KendoColumn(hidden = false, title = "User", Order = 3, filterable = true)]
        public string NameUser { get; set; }

        [KendoColumn(hidden = true)]
        public List<clsObjectRel> RelatedItems { get; set; } = new List<clsObjectRel>();

        private LogItem undoItem;

        public LogItem GetUndoItem()
        {
            
            return undoItem;
        }
        public void SetUndoItem()
        {
            undoItem = (LogItem)this.MemberwiseClone();
        }

        public LogItem()
        {
            SetUndoItem();
        }

        public LogItem(string name, string message, clsOntologyItem userItem, clsOntologyItem logState)
        {
            NameLogEntry = name;
            Message = message;
            IdUser = userItem.GUID;
            NameUser = userItem.Name;
            IdLogState = logState.GUID;
            NameLogState = logState.Name;
            SetUndoItem();
        }

    }
}
