﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;

namespace LogModule.Models
{
    public class CheckUrlsResult
    {
        public List<ResultItem<clsOntologyItem>> UrlChecks { get; set; } = new List<ResultItem<clsOntologyItem>>();
    }
}
