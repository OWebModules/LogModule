﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class ImportCubewareLogsRequest
    {
        public string IdConfig { get; private set; }
        public CancellationToken CancellationToken { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public ImportCubewareLogsRequest(string idConfig, CancellationToken cancellationToken)
        {
            IdConfig = idConfig;
            CancellationToken = cancellationToken;
        }
    }
}
