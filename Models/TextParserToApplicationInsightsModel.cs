﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class TextParserToApplicationInsightsModel
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ConfigsToInstrumentationKeys { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigsToTextParsers { get; set; } = new List<clsObjectRel>();
    }
}
