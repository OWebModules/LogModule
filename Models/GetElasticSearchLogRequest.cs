﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class GetElasticSearchLogRequest
    {
        public string IdReference { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public GetElasticSearchLogRequest(string idReference)
        {
            IdReference = idReference;
        }
    }
}
