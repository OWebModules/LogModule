﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class ElasticsearchLog
    {
        public string IdAttributeIsActive { get; set; }
        public bool IsActive { get; set; }
        public string IdAttributeItemsCountBeforeLogging { get; set; }
        public long ItemsCountBeforeLogging { get; set; }

        public string IdEsIndex { get; set; }
        public string EsIndex { get; set; }

        public string IdEsServer { get; set; }
        public string EsServer { get; set; }

        public string IdEsPort { get; set; }
        public long EsPort { get; set; }

        public string IdEsType { get; set; }
        public string EsType { get; set; }

    }
}
