﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class GetElasticSearchLogModel
    {
        public clsOntologyItem Reference { get; set; }
        public clsOntologyItem RelationType { get; set; }
        public clsOntologyItem Direction { get; set; }
        public clsObjectRel LogOfReference { get; set; }
        public clsOntologyItem ElasticSearchLog { get; set; }

        public clsObjectAtt IsActive { get; set; }
        public clsObjectAtt CountBeforeIndex { get; set; }
        public clsObjectRel LogToElasticSearchIndex { get; set; } 
        public clsObjectRel LogToElasticSearchType { get; set; }

        public clsObjectRel ElasticSearchIndexToServerPort { get; set; }
        public clsObjectRel ServerPortToServer { get; set; }

        public clsObjectRel ServerPortToPort { get; set; }
    }
}
