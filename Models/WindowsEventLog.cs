﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Models
{
    public class WindowsEventLog
    {
        public string Log { get; set; }
        public EventLogSession Session {get; set; }
        public EventLogReader LogReader { get; set; }
    }
}
