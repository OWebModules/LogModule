﻿using LogModule.Factories;
using LogModule.Models;
using LogModule.Services;
using LogModule.Validation;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using TextParserModule;

namespace LogModule
{
    public enum LogType
    {
        Success = 0,
        Error = 1
    }
    public class LogController : AppController
    {

        public clsOntologyItem ClassLogEntry => Config.LocalData.Class_Logentry;

        public async Task<ResultItem<SyncTextParserToApplicationInsightsResult>> SyncTextParserToApplicationInsights(SyncTextParserToApplicationInsightsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncTextParserToApplicationInsightsResult>>(async () =>
           {
               var result = new ResultItem<SyncTextParserToApplicationInsightsResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new SyncTextParserToApplicationInsightsResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");

               result.ResultState = ValidationController.ValidateSyncTextParserToApplicationInsightsRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               var elasticSearch = new ServiceAgentElastic(Globals);

               request.MessageOutput?.OutputInfo("Get Model...");
               var modelResult = await elasticSearch.GetTextParserToApplicationInsightsModel(request);

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have Model.");

               var textParserController = new TextParserController(Globals);


               foreach (var config in modelResult.Result.Configs)
               {
                   request.MessageOutput?.OutputInfo($"Config: {config.Name}");
                   var syncId = Guid.NewGuid().ToString();
                   var textParserRel = modelResult.Result.ConfigsToTextParsers.FirstOrDefault(configToTextParser => configToTextParser.ID_Object == config.GUID);
                   var instrumentationKeyRel = modelResult.Result.ConfigsToInstrumentationKeys.FirstOrDefault(configToInstrumentationKey => configToInstrumentationKey.ID_Object == config.GUID);
                   var instrumentationKey = instrumentationKeyRel.Name_Other;

                   var telemetryClient = new TelemetryClient();
                   telemetryClient.InstrumentationKey = instrumentationKey;

                   request.MessageOutput?.OutputInfo("Get Textparser...");
                   var getTextParserResult = await textParserController.GetTextParser(new clsOntologyItem
                   {
                       GUID = textParserRel.ID_Other,
                       Name = textParserRel.Name_Other,
                       GUID_Parent = textParserRel.ID_Parent_Other,
                       Type = textParserRel.Ontology
                   });

                   result.ResultState = getTextParserResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var textParser = getTextParserResult.Result.FirstOrDefault();

                   if (textParser == null)
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "No Textparser found!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   request.MessageOutput?.OutputInfo($"Have Textparser: {getTextParserResult.Result.First().NameParser}");

                   var userAppDB = new ElasticSearchNestConnector.clsUserAppDBSelector(textParser.NameServer, textParser.Port, textParser.NameIndexElasticSearch, Globals.SearchRange, Globals.Session);
                   var page = 0;
                   var getDocumentsResult = userAppDB.GetData_Documents(500, textParser.NameIndexElasticSearch, query: "*", strType: textParser.NameEsType, page: page);
                   if (!getDocumentsResult.IsOK)
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = getDocumentsResult.ErrorMessage;
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   long documentCount = getDocumentsResult.Documents.Count;
                   while (documentCount < getDocumentsResult.TotalCount)
                   {
                       page++;
                       getDocumentsResult = userAppDB.GetData_Documents(100, textParser.NameIndexElasticSearch, query: "*", strType: textParser.NameEsType, scrollId: getDocumentsResult.ScrollId, page: page);
                       if (!getDocumentsResult.IsOK)
                       {
                           result.ResultState = Globals.LState_Error.Clone();
                           result.ResultState.Additional1 = getDocumentsResult.ErrorMessage;
                           request.MessageOutput?.OutputError(result.ResultState.Additional1);
                           return result;
                       }


                       foreach (var doc in getDocumentsResult.Documents)
                       {
                           var dict = doc.Dict;
                           var eventTelemetric = new TraceTelemetry();
                           eventTelemetric.Timestamp = DateTime.Now;
                           eventTelemetric.Properties.Add("SyncId", syncId);

                           foreach (var key in dict.Keys)
                           {
                               var value = "";
                               var valueObj = dict[key];
                               if (valueObj != null)
                               {
                                   if (valueObj is DateTime)
                                   {
                                       value = ((DateTime)valueObj).ToString("yyyy-MM-dd HH:mm:ss");
                                   }
                               }
                               eventTelemetric.Properties.Add(key, value);
                           }

                           telemetryClient.TrackTrace(eventTelemetric);
                       }
                       telemetryClient.Flush();

                       documentCount += getDocumentsResult.Documents.Count;
                   }


               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<SyncLogEntriesToAppInsightResult>> SyncLogEntriesToAppInsight(SyncLogEntriesToAppInsightRequest request)
        {
            var taskResult = await Task.Run<ResultItem<SyncLogEntriesToAppInsightResult>>(async () =>
           {
               var result = new ResultItem<SyncLogEntriesToAppInsightResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new SyncLogEntriesToAppInsightResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateSyncLogEntriesToAppInsightRequest(request, Globals);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               var elasticAgent = new ServiceAgentElastic(Globals);

               request.MessageOutput?.OutputInfo("Get model...");
               var modelResult = await elasticAgent.GetLogentriesToApplicationInsightsModel(request);

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have model.");

               foreach (var config in modelResult.Result.Configs)
               {
                   request.MessageOutput?.OutputInfo($"Config: {config.Name}");
                   var oInstrumentationKey = modelResult.Result.InstrumentationKeysOfConfigs.SingleOrDefault(inst => inst.ID_Object == config.GUID);

                   var instrumentationKey = oInstrumentationKey.Name_Other;

                   var telemetryClient = new TelemetryClient();
                   telemetryClient.InstrumentationKey = instrumentationKey;

                   var oHost = modelResult.Result.HostsOfConfigs.FirstOrDefault(host => host.ID_Object == config.GUID);
                   var logs = modelResult.Result.EventLogsOfConfigs.Where(log => log.ID_Object == config.GUID);

                   var securityController = new SecurityModule.SecurityController(Globals);
                   if (oHost == null)
                   {
                       foreach (var log in logs)
                       {
                           request.MessageOutput?.OutputInfo("Get Logs...");
                           var eventLogs = EventLog.GetEventLogs();
                           request.MessageOutput?.OutputInfo("Have Logs.");

                           request.MessageOutput?.OutputInfo($"Get {log.Name_Other}-Logs...");
                           var applicationLog = eventLogs.FirstOrDefault(evt => evt.Log == log.Name_Other);
                           if (applicationLog == null)
                           {
                               result.ResultState = Globals.LState_Error.Clone();
                               result.ResultState.Additional1 = "Log cannot be found!";
                               request.MessageOutput?.OutputError(result.ResultState.Additional1);
                               return result;
                           }

                           var entries = applicationLog.Entries;

                           result.ResultState = SyncLogEntries(entries, telemetryClient, "Application", request.MessageOutput);
                           if (result.ResultState.GUID == Globals.LState_Error.GUID)
                           {
                               return result;
                           }

                           var clearLog = modelResult.Result.DeleteLogsOfConfigs.FirstOrDefault(del => del.ID_Object == config.GUID);
                           if (clearLog != null && clearLog.Val_Bool.Value)
                           {
                               request.MessageOutput?.OutputInfo("Clear log...");
                               applicationLog.Clear();
                               request.MessageOutput?.OutputInfo("Cleared log.");
                           }
                       }
                   }
                   else
                   {
                       var clearLog = modelResult.Result.DeleteLogsOfConfigs.FirstOrDefault(del => del.ID_Object == config.GUID);


                       foreach (var log in logs)
                       {
                           var host = oHost.Name_Other;
                           var user = "";
                           var domain = "";
                           var oUser = modelResult.Result.UsersOfConfigs.SingleOrDefault(usr => usr.ID_Object == config.GUID);
                           if (oUser != null)
                           {
                               if (oUser.Name_Other.Contains(@"\"))
                               {
                                   var splitUser = oUser.Name_Other.Split('\\');
                                   domain = splitUser[0];
                                   user = splitUser[1];
                               }
                               else
                               {
                                   user = oUser.Name_Other;
                               }
                           }

                           var oDomain = modelResult.Result.DomainsOfConfigs.SingleOrDefault(dom => dom.ID_Object == config.GUID);

                           if (oDomain != null)
                           {
                               domain = oDomain.Name_Other;
                           }

                           var passwordItemResult = await securityController.GetPassword(new clsOntologyItem
                           {
                               GUID = oUser.ID_Other,
                               Name = oUser.Name_Other,
                               GUID_Parent = oUser.ID_Parent_Other,
                               Type = oUser.Ontology
                           }, request.Password);

                           result.ResultState = passwordItemResult.Result;

                           if (result.ResultState.GUID == Globals.LState_Error.GUID)
                           {
                               return result;
                           }

                           var logReaderResult = GetLogReader(host, domain, user, passwordItemResult.CredentialItems.First().Password.Name_Other, log.Name_Other);
                           result.ResultState = SyncLogEntries(logReaderResult.Result.LogReader, telemetryClient, logReaderResult.Result.Log, request.MessageOutput);

                           if (result.ResultState.GUID == Globals.LState_Error.GUID)
                           {
                               request.MessageOutput?.OutputError(result.ResultState.Additional1);
                               return result;
                           }

                           if (clearLog != null && clearLog.Val_Bool.Value)
                           {
                               logReaderResult.Result.Session.ClearLog(logReaderResult.Result.Log);
                           }

                       }

                   }


               }











               //request.MessageOutput?.OutputInfo("Get System-Logs...");
               //var systemLog = eventLogs.FirstOrDefault(evt => evt.Log == "Application");
               //if (systemLog == null)
               //{
               //    result.ResultState = Globals.LState_Error.Clone();
               //    result.ResultState.Additional1 = "Log cannot be found!";
               //    request.MessageOutput?.OutputError(result.ResultState.Additional1);
               //    return result;
               //}

               //entries = systemLog.Entries;

               //SyncLogEntries(entries, telemetryClient, "System", request.MessageOutput);


               return result;
           });

            return taskResult;
        }


        private ResultItem<WindowsEventLog> GetLogReader(string remoteComputername, string domain, string user, string password, string log)
        {
            var result = new ResultItem<WindowsEventLog>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new WindowsEventLog()
            };

            var queryString = "<QueryList>" +
                "  <Query Id=\"0\" Path=\"Application\">" +
                $"    <Select Path=\"{log}\">" +
                "        *[System[(Level=1  or Level=2 or Level=3 or Level=4) and" +
                "        TimeCreated[timediff(@SystemTime) &lt;= 86400000]]]" +
                "    </Select>" +
                "  </Query>" +
                "</QueryList>";
            SecureString pw = new NetworkCredential($@"{domain}\{user}", password).SecurePassword;

            EventLogSession session = new EventLogSession(
                remoteComputername,                               // Remote Computer
                domain,                                  // Domain
                user,                                // Username
                pw,
                SessionAuthentication.Default);

            pw.Dispose();

            // Query the Application log on the remote computer.
            EventLogQuery query = new EventLogQuery(log, PathType.LogName, queryString);
            query.Session = session;

            try
            {
                EventLogReader logReader = new EventLogReader(query);
                result.Result.LogReader = logReader;
                result.Result.Log = log;
                result.Result.Session = session;
            }
            catch (EventLogException e)
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = e.Message;
            }

            return result;
        }

        private clsOntologyItem SyncLogEntries(EventLogReader reader, TelemetryClient telemetryClient, string log, IMessageOutput messageOutput)
        {
            var packageCount = 0;
            var machineName = Environment.MachineName;
            var result = Globals.LState_Success.Clone();

            try
            {
                var logEntry = reader.ReadEvent();
                while (logEntry != null)
                {
                    var properties = new Dictionary<string, string>();

                    var eventTelemetric = new TraceTelemetry();
                    eventTelemetric.Timestamp = logEntry.TimeCreated ?? DateTime.MinValue;
                    try
                    {
                        eventTelemetric.Message = logEntry.FormatDescription();
                        eventTelemetric.Properties.Add("EntryType", logEntry.LevelDisplayName);
                        eventTelemetric.Properties.Add("Provider", logEntry.ProviderName);
                        eventTelemetric.Properties.Add("Source", logEntry.TaskDisplayName);
                        eventTelemetric.Properties.Add("Machine", logEntry.MachineName);
                        eventTelemetric.Properties.Add("Log", log);

                        telemetryClient.TrackTrace(eventTelemetric);
                        packageCount++;
                    }
                    catch (Exception ex)
                    {
                    }



                    if ((packageCount % 500) == 0)
                    {
                        telemetryClient.Flush();
                    }
                    logEntry = reader.ReadEvent();
                }


                telemetryClient.Flush();
            }
            catch (Exception ex)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
            }
            return result;
        }

        private clsOntologyItem SyncLogEntries(EventLogEntryCollection entries, TelemetryClient telemetryClient, string log, IMessageOutput messageOutput)
        {
            var result = Globals.LState_Success.Clone();
            var packageCount = 0;
            var machineName = Environment.MachineName;
            try
            {
                messageOutput?.OutputInfo($"{entries.Count} entries...");
                foreach (EventLogEntry entry in entries)
                {
                    var properties = new Dictionary<string, string>();

                    var eventTelemetric = new TraceTelemetry();
                    eventTelemetric.Timestamp = entry.TimeWritten;
                    eventTelemetric.Message = entry.Message;
                    eventTelemetric.Properties.Add(nameof(entry.EntryType), entry.EntryType.ToString());
                    eventTelemetric.Properties.Add(nameof(entry.Source), entry.Source);
                    eventTelemetric.Properties.Add("Machine", machineName);
                    eventTelemetric.Properties.Add("Log", log);

                    telemetryClient.TrackTrace(eventTelemetric);
                    packageCount++;

                    if ((packageCount % 500) == 0)
                    {
                        messageOutput?.OutputInfo($"{packageCount} entries Done.");
                        telemetryClient.Flush();
                    }

                }
                telemetryClient.Flush();
            }
            catch (Exception ex)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                messageOutput?.OutputError(result.Additional1);

            }

            messageOutput?.OutputInfo($"{packageCount} entries Done.");

            return result;
        }

        public async Task<ResultItem<LogItem>> ChangeDateTimeStamp(LogItem logItem, DateTime dateTimeStamp)
        {
            var taskResult = await Task.Run<ResultItem<LogItem>>(() =>
           {
               var result = new ResultItem<LogItem>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = logItem
               };

               var saveAttribute = new clsObjectAtt
               {
                   ID_Attribute = logItem.IdAttributeDateTimeStamp,
                   ID_AttributeType = Config.LocalData.AttributeType_DateTimestamp.GUID,
                   ID_DataType = Config.LocalData.AttributeType_DateTimestamp.GUID_Parent,
                   ID_Class = Config.LocalData.Class_Logentry.GUID,
                   ID_Object = logItem.IdLogEntry,
                   OrderID = 1,
                   Val_Datetime = dateTimeStamp,
                   Val_Name = dateTimeStamp.ToString()
               };

               var dbWriter = new OntologyModDBConnector(Globals);
               result.ResultState = dbWriter.SaveObjAtt(new List<clsObjectAtt> { saveAttribute });

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while saving the datetimestamp!";
                   return result;
               }

               result.Result.DateTimeStamp = dateTimeStamp;

               return result;

           });
            return taskResult;
        }

        public async Task<ResultItem<List<LogItem>>> GetLogItems(clsOntologyItem refItem, bool getReferences = false)
        {
            var result = new ResultItem<List<LogItem>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<LogItem>()
            };


            var serviceAgent = new ServiceAgentElastic(Globals);
            refItem.Mark = true;
            var resultService = await serviceAgent.GetLogItemsRaw(refItem);


            result.ResultState = resultService.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var factory = new LogItemFactory();

            var factoryResult = await factory.CreateLogItemList(resultService.Result, Globals);

            result.ResultState = factoryResult.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result = factoryResult.Result;



            return result;
        }


        public async Task<ResultItem<List<LogState>>> GetLogStates()
        {
            var result = new ResultItem<List<LogState>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<LogState>()
            };
            var elasticAgent = new Services.ServiceAgentElastic(Globals);

            var resultService = await elasticAgent.GetLogStates();

            if (resultService.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }

            var factory = new LogStateFactory();

            result = await factory.CreateLogstateList(resultService.Result, Globals);

            return result;
        }

        public async Task<ResultItem<LogItem>> CreateLogEntry(clsOntologyItem logState, clsOntologyItem refItem, clsOntologyItem userItem, string message, DateTime logStamp, long orderId = 1)
        {
            var result = new ResultItem<LogItem>
            {
                ResultState = Globals.LState_Success.Clone()
            };


            var logItem = new LogItem
            {
                IdUser = userItem.GUID,
                NameUser = userItem.Name,
                NameLogEntry = logState.Name,
                IdLogState = logState.GUID,
                NameLogState = logState.Name,
                DateTimeStamp = logStamp,
                Message = message
            };

            result = await SaveLogItem(logItem, refItem, orderId);

            return result;
        }

        public async Task<ResultItem<LogItem>> CreateLogEntry(clsOntologyItem logState,
            clsOntologyItem refItem,
            clsOntologyItem userItem,
            string message,
            DateTime logStamp,
            clsOntologyItem relationType,
            clsOntologyItem direction,
            long orderId = 1)
        {
            var result = new ResultItem<LogItem>
            {
                ResultState = Globals.LState_Success.Clone()
            };


            var logItem = new LogItem
            {
                IdUser = userItem.GUID,
                NameUser = userItem.Name,
                NameLogEntry = logState.Name,
                IdLogState = logState.GUID,
                NameLogState = logState.Name,
                DateTimeStamp = logStamp,
                Message = message
            };

            result = await SaveLogItem(logItem, null, orderId);

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while saving the Logentry!";
                return result;
            }

            var relationConfig = new clsRelationConfig(Globals);
            var transaction = new clsTransaction(Globals);

            if (direction.GUID == Globals.Direction_LeftRight.GUID)
            {
                var rel = relationConfig.Rel_ObjectRelation(refItem, new clsOntologyItem
                {
                    GUID = result.Result.IdLogEntry,
                    Name = result.Result.NameLogEntry,
                    GUID_Parent = Config.LocalData.Class_Logentry.GUID,
                    Type = Globals.Type_Object
                }, relationType);

                result.ResultState = transaction.do_Transaction(rel);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while relating Logentry to Reference!";
                    return result;
                }

            }
            else
            {
                var rel = relationConfig.Rel_ObjectRelation(new clsOntologyItem
                {
                    GUID = result.Result.IdLogEntry,
                    Name = result.Result.NameLogEntry,
                    GUID_Parent = Config.LocalData.Class_Logentry.GUID,
                    Type = Globals.Type_Object
                }, refItem, relationType);

                result.ResultState = transaction.do_Transaction(rel);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while relating Logentry to Reference!";
                    return result;
                }
            }

            return result;
        }

        public async Task<ResultItem<LogItem>> CreateLogEntry(LogType logType, clsOntologyItem refItem, clsOntologyItem userItem)
        {
            var result = new ResultItem<LogItem>
            {
                ResultState = Globals.LState_Success.Clone()
            };

            var logState = Globals.LState_Success.Clone();
            switch (logType)
            {

                case LogType.Error:
                    logState = Globals.LState_Error.Clone();
                    break;


            }

            var logItem = new LogItem
            {
                IdUser = userItem.GUID,
                NameUser = userItem.Name,
                NameLogEntry = logState.Name,
                IdLogState = logState.GUID,
                NameLogState = logState.Name,
                DateTimeStamp = DateTime.Now,
                Message = logState.Name
            };

            result = await SaveLogItem(logItem, refItem);

            return result;
        }


        public async Task<clsOntologyItem> RelateLogItem(LogItem logItem, clsOntologyItem refItem, long orderId = 1)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var elasticAgent = new Services.ServiceAgentElastic(Globals);

                var logEntry = new clsOntologyItem
                {
                    GUID = logItem.IdLogEntry,
                    Name = logItem.NameLogEntry,
                    GUID_Parent = Config.LocalData.Class_Logentry.GUID,
                    Type = Globals.Type_Object
                };

                result = await elasticAgent.SaveLogEntryToRefItem(logEntry, refItem, orderId);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while relating Logentry to RefItem";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<LogItem>> SaveLogItem(LogItem logItem, clsOntologyItem refItem, long orderId = 1)
        {
            var result = new ResultItem<LogItem>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = logItem
            };

            var elasticAgent = new Services.ServiceAgentElastic(Globals);

            if (logItem.GetUndoItem().NameLogEntry != logItem.NameLogEntry)
            {
                clsOntologyItem logEntry = null;
                if (string.IsNullOrEmpty(logItem.IdLogEntry))
                {
                    logEntry = new clsOntologyItem
                    {
                        GUID = Globals.NewGUID,
                        Name = logItem.NameLogEntry,
                        GUID_Parent = Config.LocalData.Class_Logentry.GUID,
                        Type = Globals.Type_Object,
                        New_Item = true
                    };
                }
                else
                {
                    logEntry = new clsOntologyItem
                    {
                        GUID = logItem.IdLogEntry,
                        Name = logItem.NameLogEntry,
                        GUID_Parent = Config.LocalData.Class_Logentry.GUID,
                        Type = Globals.Type_Object,
                        New_Item = false
                    };
                }
                result.ResultState = await elasticAgent.SaveLogEntry(logEntry);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                logItem.IdLogEntry = logEntry.GUID;

                if (logEntry.New_Item.Value)
                {
                    if (refItem != null)
                    {
                        result.ResultState = await elasticAgent.SaveLogEntryToRefItem(logEntry, refItem, orderId);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }


                    var userItem = new clsOntologyItem
                    {
                        GUID = logItem.IdUser,
                        Name = logItem.NameUser,
                        GUID_Parent = Config.LocalData.Class_user.GUID,
                        Type = Globals.Type_Object
                    };

                    result.ResultState = await elasticAgent.SaveLogEntryToUserItem(logEntry, userItem);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var resultSaveDateTimeStamp = await elasticAgent.SaveDateTimeStamp(logEntry, logItem.DateTimeStamp.Value);

                    if (resultSaveDateTimeStamp.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState = resultSaveDateTimeStamp.ResultState;
                        return result;
                    }

                    logItem.IdAttributeDateTimeStamp = resultSaveDateTimeStamp.Result.ID_Attribute;
                }
                else
                {
                    if (logItem.GetUndoItem().NameLogEntry != logItem.NameLogEntry)
                    {
                        result.ResultState = await elasticAgent.SaveLogEntry(new clsOntologyItem
                        {
                            GUID = logItem.IdLogEntry,
                            Name = logItem.NameLogEntry,
                            GUID_Parent = Config.LocalData.Class_Logentry.GUID,
                            Type = Globals.Type_Object
                        });

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }

                }

                if (logItem.GetUndoItem().IdLogState != logItem.IdLogState)
                {
                    var logState = await GetObject(logItem.IdLogState);

                    if (logState.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState = logState.ResultState;
                        return result;
                    }

                    var saveLogState = await elasticAgent.SaveLogEntryToLogState(logEntry, logState.Result);

                    if (saveLogState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState = logState.ResultState;
                        return result;
                    }

                    result.Result.IdLogState = logState.Result.GUID;
                    result.Result.NameLogState = logState.Result.Name;
                }

                if (logItem.GetUndoItem().Message != logItem.Message)
                {
                    var resultSave = await elasticAgent.SaveMessage(new clsOntologyItem
                    {
                        GUID = logItem.IdLogEntry,
                        Name = logItem.NameLogEntry,
                        GUID_Parent = Config.LocalData.Class_Logentry.GUID,
                        Type = Globals.Type_Object
                    }, logItem.Message);

                    if (resultSave.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState = resultSave.ResultState;
                        return result;
                    }

                    logItem.IdAttributeMessage = resultSave.Result.ID_Attribute;

                }

                result.Result.IdLogEntry = logEntry.GUID;
            }

            logItem.SetUndoItem();

            return result;
        }

        public async Task<ResultItem<ElasticsearchLog>> GetElasticSearchLog(GetElasticSearchLogRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ElasticsearchLog>>(async () =>
           {
               var result = new ResultItem<ElasticsearchLog>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new ElasticsearchLog()
               };

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateGetElasticSearchLogRequest(request, Globals);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               var elasticAgent = new Services.ServiceAgentElastic(Globals);


               request.MessageOutput?.OutputInfo("Get model...");

               var modelResult = await elasticAgent.GetElasticSearchLogModel(request);

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have model.");

               request.MessageOutput?.OutputInfo("Create ElasticSearchLog-Item...");
               var factoryResult = await ElasticSearchLogFactory.CreateElasticSearchLog(modelResult.Result, Globals);
               result.ResultState = factoryResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               result.Result = factoryResult.Result;
               request.MessageOutput?.OutputInfo("Have ElasticSearchLog-Item.");

               return result;
           });

            return taskResult;
        }

        public clsObjectRel RelLogEntryToRef(LogItem logItem, clsOntologyItem refItem, clsRelationConfig relationConfig = null)
        {
            if (relationConfig == null)
            {
                relationConfig = new clsRelationConfig(Globals);
            }

            return relationConfig.Rel_ObjectRelation(ConvertLogItemToLogEntry(logItem), refItem, Config.LocalData.RelationType_belongs_to);

        }


        public clsOntologyItem ConvertLogItemToLogEntry(LogItem logItem)
        {
            return new clsOntologyItem
            {
                GUID = logItem.IdLogEntry,
                Name = logItem.NameLogEntry,
                GUID_Parent = Config.LocalData.Class_Logentry.GUID,
                Type = Globals.Type_Object
            };
        }

        public async Task<ResultItem<CheckUrlsResult>> CheckUrls(CheckUrlsRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<CheckUrlsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new CheckUrlsResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateCheckUrlsRequest(request, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                var elasticAgent = new ServiceAgentElastic(Globals);

                request.MessageOutput?.OutputInfo("Get model...");
                var modelResult = await elasticAgent.GetCheckUrlsModel(request);
                result.ResultState = modelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Have model. {modelResult.Result.ConfigsToUrls.Count} Urls");

                var relationConfig = new clsRelationConfig(Globals);

                var client = new WebClient();

                request.MessageOutput?.OutputInfo("Check Urls...");
                foreach (var configToUrl in modelResult.Result.ConfigsToUrls)
                {
                    if (request.CancellationToken.IsCancellationRequested)
                    {
                        request.MessageOutput?.OutputInfo("User abort");
                        return result;
                    }

                    var resultItem = new ResultItem<clsOntologyItem>
                    {
                        ResultState = Globals.LState_Success.Clone()
                    };
                    result.Result.UrlChecks.Add(resultItem);
                    try
                    {
                        var urlItem = new clsOntologyItem
                        {
                            GUID = configToUrl.ID_Other,
                            Name = configToUrl.Name_Other,
                            GUID_Parent = configToUrl.ID_Parent_Other,
                            Type = configToUrl.Ontology
                        };
                        request.MessageOutput?.OutputInfo($"Check Url {urlItem.Name}");
                        var uri = new Uri(urlItem.Name);

                        clsOntologyItem statusCodeOItem = null;
                        try
                        {
                            
                            var webResult = client.DownloadString(uri);
                            
                            statusCodeOItem = new clsOntologyItem
                            {
                                Name = "OK",
                                Val_Long = 200
                            };
                            request.MessageOutput?.OutputInfo($"Check Url: {statusCodeOItem.Name}");
                        }
                        catch (WebException ex)
                        {
                            statusCodeOItem = new clsOntologyItem
                            {
                                Name = ((HttpWebResponse)ex.Response).StatusCode.ToString(),
                                Val_Long = (long)((HttpWebResponse)ex.Response).StatusCode
                            };
                            request.MessageOutput?.OutputWarning($"Check Url: {statusCodeOItem.Name}");
                        }

                        
                        var statusCodeItemResult =
                            await elasticAgent.CheckStatusCodes(new List<clsOntologyItem> { statusCodeOItem });

                        if (statusCodeItemResult.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            resultItem.ResultState = statusCodeItemResult.ResultState;
                            continue;
                        }

                        var statusCodeItem = statusCodeItemResult.Result.FirstOrDefault();

                        if (statusCodeItem == null)
                        {
                            result.ResultState = Globals.LState_Error.Clone();
                            result.ResultState.Additional1 = "No status-code item!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        resultItem.Result = new clsOntologyItem
                        {
                            GUID = Globals.NewGUID,
                            Name = configToUrl.Name_Other,
                            GUID_Parent = UrlCheck.Config.LocalData.Class_Url_Check.GUID,
                            Type = Globals.Type_Object
                        };

                        result.ResultState =
                            await elasticAgent.SaveObjects(new List<clsOntologyItem> { resultItem.Result });

                        result.ResultState = statusCodeItemResult.ResultState;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the url-check!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        var saveDateTimeAttribute = relationConfig.Rel_ObjectAttribute(resultItem.Result,
                            UrlCheck.Config.LocalData.AttributeType_Datetimestamp__Create_, DateTime.Now);

                        result.ResultState =
                            await elasticAgent.SaveAttributes(new List<clsObjectAtt> { saveDateTimeAttribute });

                        if (resultItem.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the datetime of url-check!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        var relationsToSave = new List<clsObjectRel>();
                        var relationUrl = relationConfig.Rel_ObjectRelation(resultItem.Result, urlItem,
                            UrlCheck.Config.LocalData.RelationType_belongs_to);

                        relationsToSave.Add(relationUrl);

                        var relationStatusCode = relationConfig.Rel_ObjectRelation(resultItem.Result, statusCodeItem,
                            UrlCheck.Config.LocalData.RelationType_belonging);

                        relationsToSave.Add(relationStatusCode);

                        var relationUrlCheck = relationConfig.Rel_ObjectRelation(modelResult.Result.Config, resultItem.Result,
                            UrlCheck.Config.LocalData.RelationType_result);

                        relationsToSave.Add(relationUrlCheck);

                        result.ResultState =
                            await elasticAgent.SaveRelations(relationsToSave);

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the relations of Url-Check!";
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                    }
                    catch (Exception ex)
                    {
                        resultItem.ResultState = Globals.LState_Error.Clone();
                        resultItem.ResultState.Additional1 = ex.Message;
                    }

                    result.Result.UrlChecks.Add(resultItem);


                }

                request.MessageOutput?.OutputInfo("Checked Urls.");

                return result;
            });

            return taskResult;
        }

        public LogController(Globals globals) : base(globals)
        {
        }
    }
}
