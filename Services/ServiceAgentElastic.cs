﻿using LogModule.Models;
using LogModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogModule.Services
{
    public class ServiceAgentElastic : ElasticBaseAgent
    {

        public async Task<ResultItem<GetElasticSearchLogModel>> GetElasticSearchLogModel(GetElasticSearchLogRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetElasticSearchLogModel>>(async () =>
           {
               var result = new ResultItem<GetElasticSearchLogModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetElasticSearchLogModel()
               };

               result.ResultState = ValidationController.ValidateGetElasticSearchLogRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchReferenceResult = await GetOItem(request.IdReference, globals.Type_Object);

               result.ResultState = searchReferenceResult.ResultState;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Reference!";
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               result.Result.Reference = searchReferenceResult.Result;

               result.ResultState = ValidationController.ValidateGetElasticSearchLogModel(result.Result, globals, nameof(Models.GetElasticSearchLogModel.Reference));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               
               if (result.Result.Reference.GUID_Parent == ElasticSearch.Config.LocalData.Class_Log__ElasticSearch_.GUID)
               {
                   result.Result.ElasticSearchLog = result.Result.Reference;
               }

               if (result.Result.ElasticSearchLog == null)
               {
                   var searchLogOfReference = new List<clsObjectRel>
                   {
                       new clsObjectRel
                       {
                           ID_Object = result.Result.Reference.GUID,
                           ID_Parent_Other = ElasticSearch.Config.LocalData.Class_Log__ElasticSearch_.GUID
                       },
                       new clsObjectRel
                       {
                           ID_Other = result.Result.Reference.GUID,
                           ID_Parent_Object = ElasticSearch.Config.LocalData.Class_Log__ElasticSearch_.GUID
                       }
                   };

                   var dbReaderLogOfReference = new OntologyModDBConnector(globals);

                   result.ResultState = dbReaderLogOfReference.GetDataObjectRel(searchLogOfReference);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Err while getting the relation between the Reference and the Log-Item!";
                       return result;
                   }

                   result.Result.LogOfReference = dbReaderLogOfReference.ObjectRels.FirstOrDefault();

                   result.ResultState = ValidationController.ValidateGetElasticSearchLogModel(result.Result, globals, nameof(Models.GetElasticSearchLogModel.LogOfReference));

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }

                   if (result.Result.LogOfReference.ID_Parent_Other == LogModule.ElasticSearch.Config.LocalData.Class_Log__ElasticSearch_.GUID)
                   {
                       result.Result.Direction = globals.Direction_LeftRight;
                       result.Result.ElasticSearchLog = new clsOntologyItem
                       {
                           GUID = result.Result.LogOfReference.ID_Other,
                           Name = result.Result.LogOfReference.Name_Other,
                           GUID_Parent = result.Result.LogOfReference.ID_Parent_Other,
                           Type = result.Result.LogOfReference.Ontology
                       };
                   }
                   else
                   {
                       result.Result.Direction = globals.Direction_RightLeft;
                       result.Result.ElasticSearchLog = new clsOntologyItem
                       {
                           GUID = result.Result.LogOfReference.ID_Object,
                           Name = result.Result.LogOfReference.Name_Object,
                           GUID_Parent = result.Result.LogOfReference.ID_Parent_Object,
                           Type = result.Result.LogOfReference.Ontology
                       };
                   }

                   result.Result.RelationType = new clsOntologyItem
                   {
                       GUID = result.Result.LogOfReference.ID_RelationType,
                       Name = result.Result.LogOfReference.Name_RelationType,
                       Type = globals.Type_RelationType
                   };
               }

               var searchAttributes = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = result.Result.ElasticSearchLog.GUID,
                       ID_AttributeType = ElasticSearch.Config.LocalData.AttributeType_is_Active.GUID
                   },
                   new clsObjectAtt
                   {
                       ID_Object = result.Result.ElasticSearchLog.GUID,
                       ID_AttributeType = ElasticSearch.Config.LocalData.AttributeType_ItemCountBeforeLogging.GUID
                   }
               };

               var dbReaderAttributes = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the ElasticSearchLog-Attributes!";
                   return result;
               }

               result.Result.IsActive = dbReaderAttributes.ObjAtts.FirstOrDefault(att => att.ID_AttributeType == ElasticSearch.Config.LocalData.AttributeType_is_Active.GUID);

               result.ResultState = ValidationController.ValidateGetElasticSearchLogModel(result.Result, globals, nameof(Models.GetElasticSearchLogModel.IsActive));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result.CountBeforeIndex = dbReaderAttributes.ObjAtts.FirstOrDefault(att => att.ID_AttributeType == ElasticSearch.Config.LocalData.AttributeType_ItemCountBeforeLogging.GUID);

               result.ResultState = ValidationController.ValidateGetElasticSearchLogModel(result.Result, globals, nameof(Models.GetElasticSearchLogModel.CountBeforeIndex));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchLogRelations = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ElasticSearchLog.GUID,
                       ID_RelationType = ElasticSearch.Config.LocalData.ClassRel_Log__ElasticSearch__Logging_Indexes__Elastic_Search_.ID_RelationType,
                       ID_Parent_Other = ElasticSearch.Config.LocalData.ClassRel_Log__ElasticSearch__Logging_Indexes__Elastic_Search_.ID_Class_Right
                   },
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ElasticSearchLog.GUID,
                       ID_RelationType = ElasticSearch.Config.LocalData.ClassRel_Log__ElasticSearch__Logging_Types__Elastic_Search_.ID_RelationType,
                       ID_Parent_Other = ElasticSearch.Config.LocalData.ClassRel_Log__ElasticSearch__Logging_Types__Elastic_Search_.ID_Class_Right
                   }
               };

               var dbReaderLogRelations = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderLogRelations.GetDataObjectRel(searchLogRelations);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relations of the Log-Item!";
                   return result;
               }

               result.Result.LogToElasticSearchIndex = dbReaderLogRelations.ObjectRels.FirstOrDefault(rel => rel.ID_Parent_Other ==
                            ElasticSearch.Config.LocalData.ClassRel_Log__ElasticSearch__Logging_Indexes__Elastic_Search_.ID_Class_Right);

               result.ResultState = ValidationController.ValidateGetElasticSearchLogModel(result.Result, globals, nameof(Models.GetElasticSearchLogModel.LogToElasticSearchIndex));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result.LogToElasticSearchType= dbReaderLogRelations.ObjectRels.FirstOrDefault(rel => rel.ID_Parent_Other ==
                            ElasticSearch.Config.LocalData.ClassRel_Log__ElasticSearch__Logging_Types__Elastic_Search_.ID_Class_Right);

               result.ResultState = ValidationController.ValidateGetElasticSearchLogModel(result.Result, globals, nameof(Models.GetElasticSearchLogModel.LogToElasticSearchType));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchServerPort = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.LogToElasticSearchIndex.ID_Other,
                       ID_RelationType = ElasticSearch.Config.LocalData.ClassRel_Indexes__Elastic_Search__belongs_to_Server_Port.ID_RelationType,
                       ID_Parent_Other = ElasticSearch.Config.LocalData.ClassRel_Indexes__Elastic_Search__belongs_to_Server_Port.ID_Class_Right
                   }
               };

               var dbReaderServerPort = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderServerPort.GetDataObjectRel(searchServerPort);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the ServerPort!";
                   return result;
               }

               result.Result.ElasticSearchIndexToServerPort = dbReaderServerPort.ObjectRels.FirstOrDefault();

               result.ResultState = ValidationController.ValidateGetElasticSearchLogModel(result.Result, globals, nameof(Models.GetElasticSearchLogModel.ElasticSearchIndexToServerPort));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchServerPortRelations = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ElasticSearchIndexToServerPort.ID_Other,
                       ID_RelationType = ElasticSearch.Config.LocalData.ClassRel_Server_Port_belonging_Source_Server.ID_RelationType,
                       ID_Parent_Other = ElasticSearch.Config.LocalData.ClassRel_Server_Port_belonging_Source_Server.ID_Class_Right
                   },
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ElasticSearchIndexToServerPort.ID_Other,
                       ID_RelationType = ElasticSearch.Config.LocalData.ClassRel_Server_Port_belonging_Source_Port.ID_RelationType,
                       ID_Parent_Other = ElasticSearch.Config.LocalData.ClassRel_Server_Port_belonging_Source_Port.ID_Class_Right
                   }
               };

               var dbReaderServerPortRelations = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderServerPortRelations.GetDataObjectRel(searchServerPortRelations);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Server!";
                   return result;
               }

               result.Result.ServerPortToServer = dbReaderServerPortRelations.ObjectRels.FirstOrDefault(rel => rel.ID_Parent_Other == ElasticSearch.Config.LocalData.ClassRel_Server_Port_belonging_Source_Server.ID_Class_Right);

               result.ResultState = ValidationController.ValidateGetElasticSearchLogModel(result.Result, globals, nameof(Models.GetElasticSearchLogModel.ServerPortToServer));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               result.Result.ServerPortToPort = dbReaderServerPortRelations.ObjectRels.FirstOrDefault(rel => rel.ID_Parent_Other == ElasticSearch.Config.LocalData.ClassRel_Server_Port_belonging_Source_Port.ID_Class_Right);

               result.ResultState = ValidationController.ValidateGetElasticSearchLogModel(result.Result, globals, nameof(Models.GetElasticSearchLogModel.ServerPortToPort));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               return result;
           });
            return taskResult;
        }

        public async Task<ResultItem<TextParserToApplicationInsightsModel>> GetTextParserToApplicationInsightsModel(SyncTextParserToApplicationInsightsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<TextParserToApplicationInsightsModel>>(() =>
            {
                var result = new ResultItem<TextParserToApplicationInsightsModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TextParserToApplicationInsightsModel()
                };

                result.ResultState = ValidationController.ValidateSyncTextParserToApplicationInsightsRequest(request, globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchRootConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = ApplicationInsights.TextParser.Config.LocalData.Class_TextParser_to_Application_Insights.GUID
                    }
                };

                var dbReaderRootConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Root-Config!";
                    return result;
                }

                result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

                result.ResultState = ValidationController.ValidateTextParserToApplicationInsightsModel(result.Result, globals, nameof(result.Result.RootConfig));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchSubConfigs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.RootConfig.GUID,
                        ID_RelationType = ApplicationInsights.TextParser.Config.LocalData.ClassRel_TextParser_to_Application_Insights_contains_TextParser_to_Application_Insights.ID_RelationType,
                        ID_Parent_Other = ApplicationInsights.TextParser.Config.LocalData.ClassRel_TextParser_to_Application_Insights_contains_TextParser_to_Application_Insights.ID_Class_Right
                    }
                };

                var dbReaderSubConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting Sub-Configs!";
                    return result;
                }

                result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if (!result.Result.Configs.Any())
                {
                    result.Result.Configs.Add(result.Result.RootConfig);
                }

                var searchInstrumentationKeys = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = ApplicationInsights.TextParser.Config.LocalData.ClassRel_TextParser_to_Application_Insights_uses_InstrumentationKey.ID_RelationType,
                    ID_Parent_Other = ApplicationInsights.TextParser.Config.LocalData.ClassRel_TextParser_to_Application_Insights_uses_InstrumentationKey.ID_Class_Right
                }).ToList();

                var dbReaderInstrumentationKeys = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderInstrumentationKeys.GetDataObjectRel(searchInstrumentationKeys);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the instrmentation-keys!";
                    return result;
                }

                result.Result.ConfigsToInstrumentationKeys = dbReaderInstrumentationKeys.ObjectRels;

                result.ResultState = ValidationController.ValidateTextParserToApplicationInsightsModel(result.Result, globals, nameof(result.Result.ConfigsToInstrumentationKeys));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchTextParser = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = ApplicationInsights.TextParser.Config.LocalData.ClassRel_TextParser_to_Application_Insights_belonging_Textparser.ID_RelationType,
                    ID_Parent_Other = ApplicationInsights.TextParser.Config.LocalData.ClassRel_TextParser_to_Application_Insights_belonging_Textparser.ID_Class_Right
                }).ToList();

                var dbReaderTextParser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTextParser.GetDataObjectRel(searchTextParser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Textparsers!";
                    return result;
                }

                result.Result.ConfigsToTextParsers = dbReaderTextParser.ObjectRels;

                result.ResultState = ValidationController.ValidateTextParserToApplicationInsightsModel(result.Result, globals, nameof(result.Result.ConfigsToTextParsers));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetLogentriesToApplicationInsightsModelResult>> GetLogentriesToApplicationInsightsModel(SyncLogEntriesToAppInsightRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetLogentriesToApplicationInsightsModelResult>>(() =>
           {
               var result = new ResultItem<GetLogentriesToApplicationInsightsModelResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetLogentriesToApplicationInsightsModelResult()
               };

               result.ResultState = Validation.ValidationController.ValidateSyncLogEntriesToAppInsightRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var dbReaderRootConfig = new OntologyModDBConnector(globals);

               var searchRootConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = ApplicationInsights.Config.LocalData.Class_LogEntries_to_Application_Insights.GUID
                   }
               };

               result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Root-config!";
                   return result;
               }

               result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateGetLogentriesToApplicationInsightsModelResult(result.Result, globals, nameof(result.Result.RootConfig));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_contains_LogEntries_to_Application_Insights.ID_RelationType,
                       ID_Parent_Other = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_contains_LogEntries_to_Application_Insights.ID_Class_Right
                   }
               };

               var dbReaderConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Sub-configs!";
                   return result;
               }

               result.Result.Configs = dbReaderConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();

               if (!result.Result.Configs.Any())
               {
                   result.Result.Configs.Add(result.Result.RootConfig);
               }

               var searchDeleteLog = result.Result.Configs.Select(config => new clsObjectAtt
               {
                   ID_Object = config.GUID,
                   ID_AttributeType = ApplicationInsights.Config.LocalData.AttributeType_Clear_Log.GUID
               }).ToList();

               var dbReaderDeleteLogs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderDeleteLogs.GetDataObjectAtt(searchDeleteLog);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while reading the Delete-Logs info!";
                   return result;
               }

               result.Result.DeleteLogsOfConfigs = dbReaderDeleteLogs.ObjAtts;

               var searchInstrumentationKeys = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_uses_Accesstoken.ID_RelationType,
                   ID_Parent_Other = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_uses_Accesstoken.ID_Class_Right
               }).ToList();

               var dbReaderInstrumentationKeys = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderInstrumentationKeys.GetDataObjectRel(searchInstrumentationKeys);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while reading the Instrumentation-Keys!";
                   return result;
               }

               result.Result.InstrumentationKeysOfConfigs = dbReaderInstrumentationKeys.ObjectRels;

               result.ResultState = ValidationController.ValidateGetLogentriesToApplicationInsightsModelResult(result.Result, globals, nameof(result.Result.InstrumentationKeysOfConfigs));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchEventLogs = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_belonging_Source_Event_Log.ID_RelationType,
                   ID_Parent_Other = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_belonging_Source_Event_Log.ID_Class_Right
               }).ToList();

               var dbReaderLogFiles = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderLogFiles.GetDataObjectRel(searchEventLogs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while reading the Event-Logs to analyze!";
                   return result;
               }

               result.Result.EventLogsOfConfigs = dbReaderLogFiles.ObjectRels;

               result.ResultState = ValidationController.ValidateGetLogentriesToApplicationInsightsModelResult(result.Result, globals, nameof(result.Result.EventLogsOfConfigs));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchHosts = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_belonging_Host.ID_RelationType,
                   ID_Parent_Other = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_belonging_Host.ID_Class_Right
               }).ToList();

               var dbReaderHosts = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderHosts.GetDataObjectRel(searchHosts);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while reading the Hosts!";
                   return result;
               }

               result.Result.HostsOfConfigs = dbReaderHosts.ObjectRels;

               result.ResultState = ValidationController.ValidateGetLogentriesToApplicationInsightsModelResult(result.Result, globals, nameof(result.Result.HostsOfConfigs));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchUsers = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_belonging_user.ID_RelationType,
                   ID_Parent_Other = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_belonging_user.ID_Class_Right
               }).ToList();

               var dbReaderUsers = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderUsers.GetDataObjectRel(searchUsers);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while reading the Users!";
                   return result;
               }

               result.Result.UsersOfConfigs = dbReaderUsers.ObjectRels;

               result.ResultState = ValidationController.ValidateGetLogentriesToApplicationInsightsModelResult(result.Result, globals, nameof(result.Result.UsersOfConfigs));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchDomains = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_belonging_Domain__Security_.ID_RelationType,
                   ID_Parent_Other = ApplicationInsights.Config.LocalData.ClassRel_LogEntries_to_Application_Insights_belonging_Domain__Security_.ID_Class_Right
               }).ToList();

               var dbReaderDomains = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderDomains.GetDataObjectRel(searchDomains);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while reading the Domains!";
                   return result;
               }

               result.Result.DomainsOfConfigs = dbReaderDomains.ObjectRels;

               result.ResultState = ValidationController.ValidateGetLogentriesToApplicationInsightsModelResult(result.Result, globals, nameof(result.Result.DomainsOfConfigs));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<LogItemsRaw>> GetLogItemsRaw(clsOntologyItem refItem)
        {
            var taskResult = await Task.Run<ResultItem<LogItemsRaw>>(() =>
            {
                var result = new ResultItem<LogItemsRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new LogItemsRaw()
                };

                var searchRefItemsAndLogEntriesLeftRight = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = refItem.GUID,
                        ID_Parent_Other = Config.LocalData.Class_Logentry.GUID
                    }
                };

                var dbReaderRefItemLogEntries = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRefItemLogEntries.GetDataObjectRel(searchRefItemsAndLogEntriesLeftRight);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.LogEntries = dbReaderRefItemLogEntries.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if (!dbReaderRefItemLogEntries.ObjectRels.Any())
                {
                    var searchRefItemsAndLogEntriesRightLeft = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = refItem.GUID,
                            ID_Parent_Object = Config.LocalData.Class_Logentry.GUID
                        }
                    };

                    result.ResultState = dbReaderRefItemLogEntries.GetDataObjectRel(searchRefItemsAndLogEntriesRightLeft);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.LogEntries = dbReaderRefItemLogEntries.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Object,
                        Name = rel.Name_Object,
                        GUID_Parent = rel.ID_Parent_Object,
                        Type = globals.Type_Object
                    }).ToList();
                }

                var searchLogStates = result.Result.LogEntries.Select(logEntry => new clsObjectRel
                {
                    ID_Object = logEntry.GUID,
                    ID_Parent_Other = Config.LocalData.ClassRel_Logentry_provides_Logstate.ID_Class_Right,
                    ID_RelationType = Config.LocalData.ClassRel_Logentry_provides_Logstate.ID_RelationType
                }).ToList();

                var dbReaderLogstates = new OntologyModDBConnector(globals);
                if (searchLogStates.Any())
                {
                    result.ResultState = dbReaderLogstates.GetDataObjectRel(searchLogStates);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.LogEntriesToLogStates = dbReaderLogstates.ObjectRels;

                var searchDateTimeStamps = result.Result.LogEntries.Select(logEntry => new clsObjectAtt
                {
                    ID_Object = logEntry.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Logentry_DateTimestamp.ID_AttributeType
                }).ToList();

                var dbReaderDateTimeStamps = new OntologyModDBConnector(globals);

                if (searchDateTimeStamps.Any())
                {
                    result.ResultState = dbReaderDateTimeStamps.GetDataObjectAtt(searchDateTimeStamps);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.DateTimeStamps = dbReaderDateTimeStamps.ObjAtts;

                var searchMessage = result.Result.LogEntries.Select(logEntry => new clsObjectAtt
                {
                    ID_Object = logEntry.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Logentry_Message.ID_AttributeType
                }).ToList();

                var dbReaderMessages = new OntologyModDBConnector(globals);


                if (searchMessage.Any())
                {
                    result.ResultState = dbReaderMessages.GetDataObjectAtt(searchMessage);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.Messages = dbReaderMessages.ObjAtts;

                var searchUsers = result.Result.LogEntries.Select(logEntry => new clsObjectRel
                {
                    ID_Object = logEntry.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Logentry_was_created_by_user.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Logentry_was_created_by_user.ID_Class_Right
                }).ToList();

                var dbReaderUsers = new OntologyModDBConnector(globals);

                if (searchUsers.Any())
                {
                    result.ResultState = dbReaderUsers.GetDataObjectRel(searchUsers);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.LogEntriesToUsers = dbReaderUsers.ObjectRels;

                if (refItem.Mark != null && refItem.Mark.Value)
                {
                    var searchRelatedItems = result.Result.LogEntries.Select(logEntry => new clsObjectRel
                    {
                        ID_Object = logEntry.GUID,
                        ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID
                    }).ToList();

                    var dbReaderRelated = new OntologyModDBConnector(globals);

                    if (searchRelatedItems.Any())
                    {
                        result.ResultState = dbReaderRelated.GetDataObjectRel(searchRelatedItems);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result.RefItemsAndLogEntries = dbReaderRelated.ObjectRels;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveLogEntry(clsOntologyItem logEntry)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);

                result = transaction.do_Transaction(logEntry);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveLogEntryToRefItem(clsOntologyItem logEntry, clsOntologyItem refItem, long orderId)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);

                var relationConfig = new clsRelationConfig(globals);

                var rel = relationConfig.Rel_ObjectRelation(logEntry, refItem, Config.LocalData.RelationType_belongs_to, orderId: orderId);

                result = transaction.do_Transaction(rel);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveLogEntryToLogState(clsOntologyItem logEntry, clsOntologyItem logState)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);

                var relationConfig = new clsRelationConfig(globals);

                var rel = relationConfig.Rel_ObjectRelation(logEntry, logState, Config.LocalData.RelationType_provides);

                result = transaction.do_Transaction(rel, true);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveLogEntryToUserItem(clsOntologyItem logEntry, clsOntologyItem userItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var transaction = new clsTransaction(globals);

                var relationConfig = new clsRelationConfig(globals);

                var rel = relationConfig.Rel_ObjectRelation(logEntry, userItem, Config.LocalData.RelationType_was_created_by);

                result = transaction.do_Transaction(rel, true);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetLogStates()
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var searchLogStates = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Logstate.GUID
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjects(searchLogStates);
                result.Result = dbReader.Objects1;


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsObjectAtt>> SaveDateTimeStamp(clsOntologyItem logEntry, DateTime dateTimeStamp)
        {
            var taskResult = await Task.Run<ResultItem<clsObjectAtt>>(() =>
            {
                var result = new ResultItem<clsObjectAtt>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var transaction = new clsTransaction(globals);

                var relationConfig = new clsRelationConfig(globals);

                var rel = relationConfig.Rel_ObjectAttribute(logEntry, Config.LocalData.AttributeType_DateTimestamp, dateTimeStamp);

                result.ResultState = transaction.do_Transaction(rel, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                result.Result = transaction.OItem_Last.OItemObjectAtt;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsObjectAtt>> SaveMessage(clsOntologyItem logEntry, string message)
        {
            var taskResult = await Task.Run<ResultItem<clsObjectAtt>>(() =>
            {
                var result = new ResultItem<clsObjectAtt>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var transaction = new clsTransaction(globals);

                var relationConfig = new clsRelationConfig(globals);

                var rel = relationConfig.Rel_ObjectAttribute(logEntry, Config.LocalData.AttributeType_Message, message);

                result.ResultState = transaction.do_Transaction(rel, true);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                result.Result = transaction.OItem_Last.OItemObjectAtt;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<CheckUrlsModel>> GetCheckUrlsModel(CheckUrlsRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<CheckUrlsModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CheckUrlsModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the config!";
                    return result;
                }

                result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();
                result.ResultState = ValidationController.ValidateCheckurlsModel(result.Result, globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchUrls = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object =  result.Result.Config.GUID,
                        ID_RelationType =  UrlCheck.Config.LocalData.ClassRel_UrlCheck_Check_Url.ID_RelationType,
                        ID_Parent_Other =  UrlCheck.Config.LocalData.ClassRel_UrlCheck_Check_Url.ID_Class_Right
                    }
                };

                var dbReaderConfigsToUrls = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToUrls.GetDataObjectRel(searchUrls);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Urls!";
                    return result;
                }

                result.Result.ConfigsToUrls = dbReaderConfigsToUrls.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> CheckStatusCodes(List<clsOntologyItem> statusCodes)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = statusCodes
                };

                var searchStatusCode = statusCodes.Select(code => new clsOntologyItem
                {
                    Name = code.Name,
                    GUID_Parent = UrlCheck.Config.LocalData.Class_Status_Code.GUID
                }).ToList();

                var dbReaderStatusCode = new OntologyModDBConnector(globals);
                if (searchStatusCode.Any())
                {
                    result.ResultState = dbReaderStatusCode.GetDataObjects(searchStatusCode);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the status-codes!";
                        return result;
                    }

                    foreach (var statusCodeCheck in (from statusCode in statusCodes
                        join statusCodeDb in dbReaderStatusCode.Objects1 on statusCode.Name equals statusCodeDb.Name into statusCodesDb
                        from statusCodeDb in statusCodesDb.DefaultIfEmpty()
                        select new { statusCode, statusCodeDb }))
                    {
                        if (statusCodeCheck.statusCodeDb == null)
                        {
                            statusCodeCheck.statusCode.GUID = globals.NewGUID;
                            statusCodeCheck.statusCode.GUID_Parent = UrlCheck.Config.LocalData.Class_Status_Code.GUID;
                            statusCodeCheck.statusCode.New_Item = true;
                            statusCodeCheck.statusCode.Type = globals.Type_Object;
                        }
                        else
                        {
                            statusCodeCheck.statusCode.GUID = statusCodeCheck.statusCodeDb.GUID;
                            statusCodeCheck.statusCode.GUID_Parent = statusCodeCheck.statusCodeDb.GUID_Parent;
                            statusCodeCheck.statusCode.New_Item = false;
                            statusCodeCheck.statusCode.Type = statusCodeCheck.statusCodeDb.Type;
                        }
                    }


                    var statusCodesToSave = statusCodes.Where(code => code.New_Item.Value).ToList();

                    if (statusCodesToSave.Any())
                    {
                        result.ResultState = await SaveObjects(statusCodesToSave);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the status-codes!";
                            return result;
                        }
                        statusCodesToSave.ForEach(code => code.New_Item = false);

                        var relationConfig = new clsRelationConfig(globals);

                        var attributesToSave = statusCodesToSave.Select(code =>
                            relationConfig.Rel_ObjectAttribute(code, UrlCheck.Config.LocalData.AttributeType_ID,
                                code.Val_Long.Value)).ToList();

                        result.ResultState = await SaveAttributes(attributesToSave);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the status-codes ids!";
                            return result;
                        }
                    }
                }
                

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ImportCubewareLogsModelRaw>> GetImportCubewareLogModel(ImportCubewareLogsRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<ImportCubewareLogsModelRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ImportCubewareLogsModelRaw()
                };

                result.Result.ServiceAgentElastic = this;
                result.Result.RelationConfig = new clsRelationConfig(globals);

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderConfig, globals, nameof(ImportCubewareLogsModelRaw.Config));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchExecuteMeasureAttribute = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_AttributeType = Cubeware.Config.LocalData.AttributeType_Execute_Measure.GUID
                    }
                };

                var dbReaderExecuteMeasureAttribute = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderExecuteMeasureAttribute.GetDataObjectAtt(searchExecuteMeasureAttribute);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the ExecuteMeasure Attribute!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderExecuteMeasureAttribute, globals, nameof(ImportCubewareLogsModelRaw.ConfigToExecuteMeasure));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchLastExecutions = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_contains_Last_MainJob_Import.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_contains_Last_MainJob_Import.ID_Class_Right
                    }
                };

                var dbReaderLastExecutions = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderLastExecutions.GetDataObjectRel(searchLastExecutions);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the last executions!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderLastExecutions, globals, nameof(ImportCubewareLogsModelRaw.ConfigToLastExecutes));

                var searchLastDateTimeStampAttribute = result.Result.ConfigToLastExecutes.Select(configToExecute => new clsObjectAtt
                {
                    ID_Object = configToExecute.ID_Other,
                    ID_AttributeType = Cubeware.Config.LocalData.AttributeType_Last_DateTimeStamp.GUID
                }).ToList();

                if (result.Result.ConfigToLastExecutes.Any())
                {
                    var dbReaderLastDateTimeStampAttribute = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderLastDateTimeStampAttribute.GetDataObjectAtt(searchLastDateTimeStampAttribute);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the LastDateTimeStamp Attribute!";
                        return result;
                    }

                    result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderLastDateTimeStampAttribute, globals, nameof(ImportCubewareLogsModelRaw.LastExecutesToDateTimeStamp));

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var searchLastExecutesToJobs = result.Result.ConfigToLastExecutes.Select(configToExecute => new clsObjectRel
                    {
                        ID_Object = configToExecute.ID_Other,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Last_MainJob_Import_belongs_to_Jobs__Cubeware_.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Last_MainJob_Import_belongs_to_Jobs__Cubeware_.ID_Class_Right
                    }).ToList();

                    var dbReaderLastExecutesToJobs = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderLastExecutesToJobs.GetDataObjectRel(searchLastExecutesToJobs);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the last-execution Jobs!";
                        return result;
                    }

                    result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderLastExecutesToJobs, globals, nameof(ImportCubewareLogsModelRaw.LastExecutesToJobs));

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchJobsDurationField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Jobs_Duration_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Jobs_Duration_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderJobsDurationField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderJobsDurationField.GetDataObjectRel(searchJobsDurationField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Jobs-Duration Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderJobsDurationField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToJobsDurationField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchCWStepField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_CWStep_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_CWStep_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderCWStepField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderCWStepField.GetDataObjectRel(searchCWStepField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the CWStep Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderCWStepField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToCWStepField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchJobsCaseField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Jobs_Case_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Jobs_Case_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderJobsCaseField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderJobsCaseField.GetDataObjectRel(searchJobsCaseField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Jobs-Case Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderJobsCaseField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToJobsCaseField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchJobField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Job_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Job_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderJobField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderJobField.GetDataObjectRel(searchJobField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Job Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderJobField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToJobField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchStartField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Start_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Start_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderStartField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderStartField.GetDataObjectRel(searchStartField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Start Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderStartField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToStartFieldField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchJobDurationField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Job_Duration_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Job_Duration_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderJobDurationField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderJobDurationField.GetDataObjectRel(searchJobDurationField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Start Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderJobDurationField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToJobDurationField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchScriptField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Script_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Script_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderScriptField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderScriptField.GetDataObjectRel(searchScriptField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Script Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderScriptField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToScriptField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchMappingDurationField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Mapping_Duration_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Mapping_Duration_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderMappingDurationField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMappingDurationField.GetDataObjectRel(searchMappingDurationField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Mapping-Duration Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderMappingDurationField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToMappingDurationField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchEndField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_End_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_End_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderEndField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderEndField.GetDataObjectRel(searchEndField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the End Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderEndField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToEndField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchScriptDurationField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Script_Duration_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Script_Duration_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderScriptDurationField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderScriptDurationField.GetDataObjectRel(searchScriptDurationField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Script-Duration Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderScriptDurationField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToScriptDurationField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchCWStepDurationField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_CWStep_Duration_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_CWStep_Duration_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderCWStepDurationField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderCWStepDurationField.GetDataObjectRel(searchCWStepDurationField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the CWStep-Duration Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderCWStepDurationField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToCWStepDurationField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchMappingField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Mapping_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Mapping_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderMappingField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMappingField.GetDataObjectRel(searchMappingField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Mapping Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderMappingField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToMappingField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchErrorField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Error_Field_Field.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_Error_Field_Field.ID_Class_Right
                    }
                };

                var dbReaderErrorField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderErrorField.GetDataObjectRel(searchErrorField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Error Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderErrorField, globals, nameof(ImportCubewareLogsModelRaw.ConfigToErrorField));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchTextParserMeasurement = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_belonging_Source_Textparser_Measurement.ID_RelationType,
                        ID_Parent_Other = Cubeware.Config.LocalData.ClassRel_Cubeware_Logimporter_belonging_Source_Textparser_Measurement.ID_Class_Right
                    }
                };

                var dbReaderTextParserMeasurement = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTextParserMeasurement.GetDataObjectRel(searchTextParserMeasurement);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Textparser Measurement!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderTextParserMeasurement, globals, nameof(ImportCubewareLogsModelRaw.ConfigToTextParserMeasurement));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchRessources = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Cubeware.Config.LocalData.RelationType_belonging_Resources.GUID
                    }
                };

                var dbReaderRessources = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRessources.GetDataObjectRel(searchRessources);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Mapped Ressources!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetImportCubewareLogsModel(result.Result, dbReaderRessources, globals, nameof(ImportCubewareLogsModelRaw.MappedRessources));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals) : base(globals)
        {
        }
    }

    public class LogItemsRaw
    {
        public List<clsOntologyItem> LogEntries { get; set; }
        public List<clsObjectRel> RefItemsAndLogEntries { get; set; }
        public List<clsObjectRel> LogEntriesToLogStates { get; set; }
        public List<clsObjectRel> LogEntriesToUsers { get; set; }
        public List<clsObjectAtt> DateTimeStamps { get; set; }
        public List<clsObjectAtt> Messages { get; set; }
    }
}
